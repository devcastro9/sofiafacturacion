﻿using System.Text.Json.Serialization;

namespace SofiaFacturacion.ViewModels;

public class ConsultaDetalle
{
    [JsonPropertyName("id")]
    public string IdDetalle { get; set; } = null!;

    [JsonPropertyName("concepto")]
    public string Concepto { get; set; } = null!;

    [JsonPropertyName("codigo_item")]
    public string CodigoItem { get; set; } = null!;

    [JsonPropertyName("cantidad")]
    public int Cantidad { get; set; }

    [JsonPropertyName("unidad_medida")]
    public string UnidadMedida { get; set; } = null!;

    [JsonPropertyName("costo_unitario")]
    public decimal CostoUnitario { get; set; }

    [JsonPropertyName("subtotal")]
    public decimal SubTotal { get; set; }

    [JsonPropertyName("descuento_detalle")]
    public string DescuentoDetalle { get; set; } = null!;

    [JsonPropertyName("descuentos_total")]
    public decimal DescuentosTotal { get; set; }

    [JsonPropertyName("multas_recargos_detalle")]
    public string MultasRecargosDetalle { get; set; } = null!;

    [JsonPropertyName("multas_recargos")]
    public decimal MultasRecargos { get; set; }

    [JsonPropertyName("valor_total")]
    public decimal ValorTotal { get; set; }
}
