﻿using System.Text.Json.Serialization;

namespace SofiaFacturacion.ViewModels;

public class AbstractValue
{
    [JsonPropertyName("value")]
    public bool? Valor { get; set; }

    [JsonPropertyName("text")]
    public string? Texto { get; set; }
}
