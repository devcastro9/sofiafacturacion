﻿using System;
using System.Text.Json.Serialization;

namespace SofiaFacturacion.ViewModels;

public class BcbUfvRes
{
    [JsonPropertyName("fecha")]
    public DateTime Fecha { get; set; }

    [JsonPropertyName("val_ufv")]
    [JsonNumberHandling(JsonNumberHandling.AllowReadingFromString)]
    public decimal Ufv { get; set; }
}
