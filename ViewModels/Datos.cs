﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace SofiaFacturacion.ViewModels;

public class Datos
{
    public Datos()
    {
        DetallesConsulta = new HashSet<ConsultaDetalle>();
        DetallesMetadatos = new HashSet<RegistroPagoMetadato>();
        //Facturas = new HashSet<ConsultaFactura>();
    }

    [JsonPropertyName("identificador")]
    public string IdFactura { get; set; } = null!;

    [JsonPropertyName("fecha_registro_deuda")]
    public DateTime FechaRegistroDeuda { get; set; }

    [JsonPropertyName("codigo_recaudacion")]
    public string CodigoRecaudacion { get; set; } = null!;

    [JsonPropertyName("email_cliente")]
    public string EmailCliente { get; set; } = null!;

    [JsonPropertyName("descripcion")]
    public string Descripcion { get; set; } = null!;

    [JsonPropertyName("numero_documento")]
    public string NumeroDocumento { get; set; } = null!;

    [JsonPropertyName("complemento_documento")]
    public string ComplementoDocumento { get; set; } = null!;

    [JsonPropertyName("codigo_tipo_documento")]
    public string CodigoTipoDocumento { get; set; } = null!;

    [JsonPropertyName("cliente_nombres")]
    public string ClienteNombres { get; set; } = null!;

    [JsonPropertyName("cliente_apellidos")]
    public string ClienteApellidos { get; set; } = null!;

    [JsonPropertyName("fecha_vencimiento")]
    public DateTime FechaVencimiento { get; set; }

    [JsonPropertyName("deuda_expirada")]
    public bool DeudaExpirada { get; set; }

    [JsonPropertyName("subtotal")]
    public decimal SubTotal { get; set; }

    [JsonPropertyName("descuento_global")]
    public decimal DescuentoGlobal { get; set; }

    [JsonPropertyName("total_envio")]
    public decimal TotalEnvio { get; set; }

    [JsonPropertyName("valor_total")]
    public decimal ValorTotal { get; set; }

    [JsonPropertyName("moneda")]
    public string Moneda { get; set; } = null!;

    [JsonPropertyName("lineas_detalles_deuda")]
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public virtual ICollection<ConsultaDetalle> DetallesConsulta { get; set; }

    [JsonPropertyName("lineas_metadatos")]
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public virtual ICollection<RegistroPagoMetadato> DetallesMetadatos { get; set; }

    [JsonPropertyName("facturas")]
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public virtual ICollection<ConsultaFactura>? Facturas { get; set; }
}
