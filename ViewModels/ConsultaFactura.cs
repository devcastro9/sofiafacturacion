﻿using System;
using System.Text.Json.Serialization;

namespace SofiaFacturacion.ViewModels;

public class ConsultaFactura
{
    [JsonPropertyName("cliente_nit")]
    public long ClienteNit { get; set; }

    [JsonPropertyName("cliente_razon_social")]
    public string ClienteRazonSocial { get; set; } = null!;

    [JsonPropertyName("leyenda")]
    public string Leyenda { get; set; } = null!;

    [JsonPropertyName("fecha_limite_emision")]
    public string FechaLimiteEmision { get; set; } = null!;

    [JsonPropertyName("codigo_control")]
    public string CodigoControl { get; set; } = null!;

    [JsonPropertyName("tipo_dosificacion")]
    public string TipoDosificacion { get; set; } = null!;

    [JsonPropertyName("numero_factura")]
    public int NumeroFactura { get; set; }

    [JsonPropertyName("numero_autorizacion")]
    public string NumeroAutorizacion { get; set; } = null!;

    [JsonPropertyName("fecha_generacion")]
    public DateTime FechaGeneracion { get; set; }

    [JsonPropertyName("url")]
    public string UrlLibelula { get; set; } = null!;

    [JsonPropertyName("appkey_empresa")]
    public string AppKey { get; set; } = null!;
}
