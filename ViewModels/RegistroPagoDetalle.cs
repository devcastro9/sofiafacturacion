﻿using System.Text.Json.Serialization;

namespace SofiaFacturacion.ViewModels;

public class RegistroPagoDetalle
{
    [JsonPropertyName("actividad_economica")]
    [JsonNumberHandling(JsonNumberHandling.WriteAsString)]
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public int? ActividadEconomica { get; set; }

    [JsonPropertyName("codigo_producto_sin")]
    [JsonNumberHandling(JsonNumberHandling.WriteAsString)]
    public int CodigoProductoSin { get; set; }

    [JsonPropertyName("codigo_producto")]
    //[JsonNumberHandling(JsonNumberHandling.WriteAsString)]
    public string CodigoProducto { get; set; } = null!;

    [JsonPropertyName("concepto")]
    public string Concepto { get; set; } = null!;

    [JsonPropertyName("cantidad")]
    public decimal Cantidad { get; set; }

    [JsonPropertyName("unidad_medida")]
    [JsonNumberHandling(JsonNumberHandling.WriteAsString)]
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public int? UnidadMedida { get; set; }

    [JsonPropertyName("costo_unitario")]
    public decimal CostoUnitario { get; set; }

    [JsonPropertyName("descuento_unitario")]
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public decimal? DescuentoUnitario { get; set; }

    [JsonPropertyName("descuento_detalle")]
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public string? DescuentoDetalle { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.Always)]
    public long? Cobranza { get; set; }
}
