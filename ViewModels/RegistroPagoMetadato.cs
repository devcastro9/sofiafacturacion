﻿using System.Text.Json.Serialization;

namespace SofiaFacturacion.ViewModels;

public class RegistroPagoMetadato
{
    [JsonPropertyName("nombre")]
    public string Nombre { get; set; } = null!;

    [JsonPropertyName("dato")]
    public string Dato { get; set; } = null!;
}
