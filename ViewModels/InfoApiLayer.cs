﻿using System.Text.Json.Serialization;

namespace SofiaFacturacion.ViewModels;

public class InfoApiLayer
{
    [JsonPropertyName("timestamp")]
    public long Tiempo { get; set; }

    [JsonPropertyName("rate")]
    public decimal Tasa { get; set; }
}
