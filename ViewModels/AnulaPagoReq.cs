﻿using System.Text.Json.Serialization;

namespace SofiaFacturacion.ViewModels;

public class AnulaPagoReq
{
    [JsonPropertyName("appkey")]
    public string AppKey { get; set; } = null!;

    [JsonPropertyName("identificador")]
    [JsonNumberHandling(JsonNumberHandling.WriteAsString)]
    public long IdFactura { get; set; }

    [JsonPropertyName("monto_pagado")]
    public decimal Monto { get; set; }

    [JsonPropertyName("id_transaccion_anulacion")]
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public string? IdTransaccionAnulacion { get; set; }

    [JsonPropertyName("motivo")]
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public string? Motivo { get; set; }

    [JsonPropertyName("sucursal")]
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public string? Sucursal { get; set; }

    [JsonPropertyName("cajero")]
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public string? Cajero { get; set; }

    [JsonPropertyName("caja")]
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public string? Caja { get; set; }
}
