﻿using System.Text.Json.Serialization;

namespace SofiaFacturacion.ViewModels;

public class AbstractRes
{
    [JsonPropertyName("email")]
    public string Email { get; set; } = null!;

    [JsonPropertyName("autocorrect")]
    public string AutoCorregido { get; set; } = null!;

    [JsonPropertyName("deliverability")]
    public string Entregable { get; set; } = null!;

    [JsonPropertyName("quality_score")]
    public decimal Puntuacion { get; set; }

    [JsonPropertyName("is_valid_format")]
    public virtual AbstractValue EsFormatoValido { get; set; } = null!;

    [JsonPropertyName("is_free_email")]
    public virtual AbstractValue EsEmailGratis { get; set; } = null!;

    [JsonPropertyName("is_disposable_email")]
    public virtual AbstractValue EsEmailDesechable { get; set; } = null!;

    [JsonPropertyName("is_role_email")]
    public virtual AbstractValue EsEmailRol { get; set; } = null!;

    [JsonPropertyName("is_catchall_email")]
    public virtual AbstractValue EsEmailRecepcionaTodo { get; set; } = null!;

    [JsonPropertyName("is_mx_found")]
    public virtual AbstractValue EsEmailMX { get; set; } = null!;

    [JsonPropertyName("is_smtp_valid")]
    public virtual AbstractValue EsSmtpValido { get; set; } = null!;
}
