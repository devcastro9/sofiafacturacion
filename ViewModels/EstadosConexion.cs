﻿using System.Text.Json.Serialization;

namespace SofiaFacturacion.ViewModels;

public class EstadosConexion
{
    [JsonPropertyName("internet_online")]
    public bool Internet { get; set; }
    [JsonPropertyName("impuestos_online")]
    public bool Impuestos { get; set; }
    [JsonPropertyName("libelula_online")]
    public bool Libelula { get; set; }
}
