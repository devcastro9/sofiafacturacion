﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace SofiaFacturacion.ViewModels;

public class RegistroPagoReq
{
    public RegistroPagoReq()
    {
        FacturaDetalles = new HashSet<RegistroPagoDetalle>();
        FacturaMetadatos = new HashSet<RegistroPagoMetadato>();
    }

    [JsonPropertyName("appkey")]
    public string AppKey { get; set; } = null!;

    [JsonPropertyName("identificador_deuda")]
    public long IdFactura { get; set; }

    [JsonPropertyName("email_cliente")]
    public string EmailCliente { get; set; } = null!;

    [JsonPropertyName("callback_url")]
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public string? CallbackUrl { get; set; }

    [JsonPropertyName("descripcion")]
    public string Descripcion { get; set; } = null!;

    [JsonPropertyName("razon_social")]
    public string RazonSocial { get; set; } = null!;

    [JsonPropertyName("numero_documento")]
    public string NumeroDocumento { get; set; } = null!;

    [JsonPropertyName("codigo_tipo_documento")]
    public string CodigoTipoDocumento { get; set; } = null!;

    [JsonPropertyName("codigo_cliente")]
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public string? CodigoCliente { get; set; }

    [JsonPropertyName("nombre_cliente")]
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public string? NombreCliente { get; set; }

    [JsonPropertyName("apellido_cliente")]
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public string? ApellidoCliente { get; set; }

    [JsonPropertyName("ci")]
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public string? CarnetIdentidad { get; set; }

    [JsonPropertyName("factura_nota_cliente")]
    public string? FacturaNotaCliente { get; set; }

    [JsonPropertyName("tipo_factura")]
    public string TipoFactura { get; set; } = null!;

    [JsonPropertyName("emite_factura")]
    public bool EmiteFactura { get; set; }

    [JsonPropertyName("moneda")]
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public string? Moneda { get; set; }

    [JsonPropertyName("moneda_tipo_cambio")]
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public decimal? TipoCambio { get; set; }

    [JsonPropertyName("codigo_punto_venta")]
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public string? CodigoPuntoVenta { get; set; }

    [JsonPropertyName("canal_caja")]
    public string? CanalCaja { get; set; }

    [JsonPropertyName("canal_caja_sucursal")]
    public string? CanalCajaSucursal { get; set; }

    [JsonPropertyName("canal_caja_usuario")]
    public string? CanalCajaUsuario { get; set; }

    [JsonPropertyName("descuento_global")]
    public decimal DescuentoGlobal { get; set; }

    [JsonPropertyName("metodo_pago_en_caja")]
    public string MetodoPago { get; set; }

    [JsonPropertyName("lineas_detalle_deuda")]
    public virtual ICollection<RegistroPagoDetalle> FacturaDetalles { get; set; }

    [JsonPropertyName("lineas_metadatos")]
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public virtual ICollection<RegistroPagoMetadato> FacturaMetadatos { get; set; } 
}
