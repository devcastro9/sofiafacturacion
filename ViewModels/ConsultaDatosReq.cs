﻿using System.Text.Json.Serialization;

namespace SofiaFacturacion.ViewModels;

public class ConsultaDatosReq
{
    [JsonPropertyName("appkey")]
    public string AppKey { get; set; } = null!;

    [JsonPropertyName("identificador")]
    public long IdFactura { get; set; }
}
