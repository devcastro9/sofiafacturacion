﻿using System;
using System.Text.Json.Serialization;

namespace SofiaFacturacion.ViewModels;

public class ConsultaDatosRes
{
    [JsonPropertyName("error")]
    public int Error { get; set; }

    [JsonPropertyName("existente")]
    public int Existente { get; set; }

    [JsonPropertyName("mensaje")]
    public string? Mensaje { get; set; }

    [JsonPropertyName("datos")]
    public Datos? Datos { get; set; }

    [JsonPropertyName("pagado")]
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public bool? Pagado { get; set; }

    [JsonPropertyName("fecha_pago")]
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public DateTime? FechaPago { get; set; }

    [JsonPropertyName("forma_pago")]
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public string? FormaPago { get; set; }

    [JsonPropertyName("forma_pago_codigo")]
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public string? FormaPagoCodigo { get; set; }

    [JsonPropertyName("metodo_pago")]
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public string? MetodoPago { get; set; }

    [JsonPropertyName("url_pasarela_pagos")]
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public string? UrlPasarelaPagos { get; set; }
}
