﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace SofiaFacturacion.ViewModels;

public class RegistroPagoRes
{
    public RegistroPagoRes()
    {
        FacturasElectronicas = new HashSet<FacturaElectronicaRes>();
    }

    [JsonPropertyName("error")]
    public int Error { get; set; }

    [JsonPropertyName("existente")]
    public int Existente { get; set; }

    [JsonPropertyName("mensaje")]
    public string? Mensaje { get; set; }

    [JsonPropertyName("codigo_recaudacion")]
    public string? CodigoRecaudacion { get; set; }

    [JsonPropertyName("id_transaccion")]
    public string? IdTransaccion { get; set; }

    [JsonPropertyName("facturas_electronicas")]
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public virtual ICollection<FacturaElectronicaRes> FacturasElectronicas { get; set; }

    [JsonPropertyName("url_pasarela_pagos")]
    public string? UrlPasarelaPagos { get; set; }
}
