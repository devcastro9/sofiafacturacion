﻿using System;
using System.Text.Json.Serialization;

namespace SofiaFacturacion.ViewModels;

public class ApiLayerRes
{
    [JsonPropertyName("success")]
    public bool Sucess { get; set; }

    [JsonPropertyName("query")]
    public QueryApiLayer Consulta { get; set; } = null!;

    [JsonPropertyName("info")]
    public InfoApiLayer Informacion { get; set; } = null!;

    [JsonPropertyName("date")]
    public DateTime Fecha { get; set; }

    [JsonPropertyName("historical")]
    public bool Historico { get; set; }

    [JsonPropertyName("result")]
    public decimal Resultado { get; set; }
}
