﻿using System.Text.Json.Serialization;

namespace SofiaFacturacion.ViewModels;

public class AnulaPagoRes
{
    [JsonPropertyName("proceso_exitoso")]
    public bool Estado { get; set; }

    [JsonPropertyName("mensaje")]
    public string Mensaje { get; set; } = null!;
}
