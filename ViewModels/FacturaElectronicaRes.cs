﻿using System;
using System.Text.Json.Serialization;

namespace SofiaFacturacion.ViewModels;

public class FacturaElectronicaRes
{
    [JsonPropertyName("url")]
    public string? Url { get; set; }

    [JsonPropertyName("identificador")]
    public string? Identificador { get; set; }

    [JsonPropertyName("numero_factura")]
    public long NumeroFactura { get; set; }

    [JsonPropertyName("numero_autorizacion")]
    public string? NumeroAutorizacion { get; set; }

    [JsonPropertyName("nit")]
    public string? Nit { get; set; }

    [JsonPropertyName("cliente_razon_social")]
    public string? RazonSocial { get; set; }

    [JsonPropertyName("cliente_nit")]
    public string? ClienteNit { get; set; }

    [JsonPropertyName("cliente_tipo_documento")]
    public string? ClienteTipoDocumento { get; set; }

    [JsonPropertyName("codigo_control")]
    public string? CodigoControl { get; set; }

    [JsonPropertyName("tipo_dosificacion")]
    public string? TipoDosificacion { get; set; }

    [JsonPropertyName("codigo_compania")]
    public string? CodigoCompania { get; set; }

    [JsonPropertyName("leyenda_dosificacion")]
    public string? LeyendaDosificacion { get; set; }

    [JsonPropertyName("fecha_pago_en_canal")]
    public DateTime? FechaPagoCanal { get; set; }

    [JsonPropertyName("fecha_inicio_ciclo")]
    public DateTime? FechaInicioCiclo { get; set; }

    [JsonPropertyName("fecha_final_ciclo")]
    public DateTime? FechaFinalCiclo { get; set; }

    [JsonPropertyName("eticket_dosificacion")]
    public string? EticketDosificacion { get; set; }

    [JsonPropertyName("fecha_limite_emision")]
    public DateTime? FechaLimiteEmision { get; set; }

    [JsonPropertyName("paralela_dosificacion")]
    public string? ParalelaDosificacion { get; set; }

    [JsonPropertyName("codigo_qr")]
    public string? CodigoQr { get; set; }

    [JsonPropertyName("leyenda_ley")]
    public string? LeyendaLey { get; set; }

    [JsonPropertyName("leyenda_repgrafica")]
    public string? LeyendaRepGrafica { get; set; }

    [JsonPropertyName("url_sin_sfe")]
    public string? UrlSinSfe { get; set; }

    [JsonPropertyName("cufd")]
    public string? Cufd { get; set; }
}
