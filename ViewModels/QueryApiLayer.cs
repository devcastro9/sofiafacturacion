﻿using System.Text.Json.Serialization;

namespace SofiaFacturacion.ViewModels;

public class QueryApiLayer
{
    [JsonPropertyName("amount")]
    public decimal Monto { get; set; }

    [JsonPropertyName("from")]
    public string MonedaDe { get; set; } = null!;

    [JsonPropertyName("to")]
    public string MonedaA { get; set; } = null!;
}
