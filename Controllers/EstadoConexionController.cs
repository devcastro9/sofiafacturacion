﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SofiaFacturacion.Data;
using SofiaFacturacion.Services;
using SofiaFacturacion.ViewModels;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SofiaFacturacion.Controllers;

[Route("api/[controller]")]
[ApiController]
public class EstadoConexionController : ControllerBase
{
    private readonly AppDbContext _context;
    private readonly LibelulaApi _libelulaApi;
    private readonly ITestInternet _internet;
    private readonly ISiatFacturacion _siatFacturacion;
    private readonly ILogger<EstadoConexionController> _logger;
    public EstadoConexionController(AppDbContext context, LibelulaApi libelulaApi, ITestInternet internet, ISiatFacturacion siatFacturacion, ILogger<EstadoConexionController> logger)
    {
        _context = context;
        _libelulaApi = libelulaApi;
        _internet = internet;
        _siatFacturacion = siatFacturacion;
        _logger = logger;
    }

    [HttpGet("Conexion")]
    public async Task<ActionResult<EstadosConexion>> GetEstadoConexion()
    {
        _logger.LogInformation("GET estados de conexion {DateTime.Now.ToLongTimeString()}", DateTime.Now.ToLongTimeString());
        try
        {
            string? Appkey = await (from c in _context.FacConfiguraciones where c.Estado select c.AppKey).AsNoTracking().FirstOrDefaultAsync();
            string? TokenSiat = await (from c in _context.FacConfiguraciones where c.Estado select c.TokenDelegado).AsNoTracking().FirstOrDefaultAsync();
            EstadosConexion estado = new() { Internet = await _internet.ExisteInternet(), Impuestos = await _siatFacturacion.VerificarComunicacionSiat(TokenSiat), Libelula = await _libelulaApi.EsOnline(Appkey) };
            return Ok(estado);
        }
        catch (Exception)
        {
            return BadRequest(new EstadosConexion() { Internet = false, Impuestos = false, Libelula = false });
        }
    }
}
