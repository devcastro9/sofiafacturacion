﻿using Microsoft.EntityFrameworkCore;
using SofiaFacturacion.Models;

namespace SofiaFacturacion.Data;

public partial class AppDbContext : DbContext
{
    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

    public virtual DbSet<AoVentasCobranzaFac> AoVentasCobranzaFacs { get; set; }

    public virtual DbSet<FacConfiguracion> FacConfiguraciones { get; set; }

    public virtual DbSet<FacDominioEmail> FacDominioEmails { get; set; }

    public virtual DbSet<FacEstado> FacEstados { get; set; }

    public virtual DbSet<FacEstadoTipo> FacEstadoTipos { get; set; }

    public virtual DbSet<FacKey> FacKeys { get; set; }

    public virtual DbSet<FacRegistroEvento> FacRegistroEventos { get; set; }

    public virtual DbSet<FacRegistroJson> FacRegistroJsons { get; set; }

    public virtual DbSet<FacRegistroJsonTipo> FacRegistroJsonTipos { get; set; }

    public virtual DbSet<FacturaElec> FacturaElecs { get; set; }

    public virtual DbSet<FacturaElecAnulacion> FacturaElecAnulaciones { get; set; }

    public virtual DbSet<FacturaElecConsulta> FacturaElecConsultas { get; set; }

    public virtual DbSet<FacturaElecDetalle> FacturaElecDetalles { get; set; }

    public virtual DbSet<FacturaElecMetadato> FacturaElecMetadatos { get; set; }

    public virtual DbSet<FacturaLibelulaEvento> FacturaLibelulaEventos { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // Metodo base
        base.OnModelCreating(modelBuilder);
        // Api fluent
        modelBuilder.Entity<AoVentasCobranzaFac>(entity =>
        {
            //entity.ToTable("ao_ventas_cobranza_fac", tb => tb.HasTrigger("trg_cobranza_fac"));
            entity.Property(e => e.CodigoEmpresa).HasDefaultValueSql("((0))");
            entity.Property(e => e.DebitoFiscal13Bs).HasDefaultValueSql("((0))");
            entity.Property(e => e.DebitoFiscal13Dol).HasDefaultValueSql("((0))");
            entity.Property(e => e.DosificaAutorizacion).HasDefaultValueSql("((0))");
            entity.Property(e => e.EstadoCobrado)
                .HasDefaultValueSql("('REG')")
                .IsFixedLength();
            entity.Property(e => e.EstadoCodigo)
                .HasDefaultValueSql("('REG')")
                .IsFixedLength();
            entity.Property(e => e.EstadoCodigoFac)
                .HasDefaultValueSql("('REG')")
                .IsFixedLength();
            entity.Property(e => e.EstadoContab)
                .HasDefaultValueSql("('REG')")
                .IsFixedLength();
            entity.Property(e => e.EstadoFac)
                .HasDefaultValueSql("('REG')")
                .IsFixedLength();
            entity.Property(e => e.Factura87Bs).HasDefaultValueSql("((0))");
            entity.Property(e => e.Factura87Dol).HasDefaultValueSql("((0))");
            entity.Property(e => e.FacturaImpresa).IsFixedLength();
            entity.Property(e => e.NroFactura).HasDefaultValueSql("((0))");
            entity.Property(e => e.SolicitudTipo).HasDefaultValueSql("((0))");
            entity.Property(e => e.TipoBienServicio).HasDefaultValueSql("((2))");
        });

        modelBuilder.Entity<FacConfiguracion>(entity =>
        {
            entity.Property(e => e.DtLastUpdateDt).HasDefaultValueSql("(getdate())");
        });

        modelBuilder.Entity<FacDominioEmail>(entity =>
        {
            entity.Property(e => e.Habilitado).HasDefaultValueSql("((0))");
        });

        modelBuilder.Entity<FacEstado>(entity =>
        {
            entity.Property(e => e.EsModificable).HasDefaultValueSql("((0))");
            entity.Property(e => e.Habilitado).HasDefaultValueSql("((0))");

            entity.HasOne(d => d.TipoFac).WithMany(p => p.FacEstados).HasConstraintName("FK_facEstado_facEstadoTipo");
        });

        modelBuilder.Entity<FacRegistroEvento>(entity =>
        {
            entity.ToTable("facRegistroEvento", tb => tb.HasTrigger("trg_registro_evento"));

            entity.Property(e => e.DtCreationDt).HasDefaultValueSql("(getdate())");
            entity.Property(e => e.Error).HasDefaultValueSql("((1))");
        });

        modelBuilder.Entity<FacRegistroJson>(entity =>
        {
            entity.ToTable("facRegistroJson", tb => tb.HasTrigger("trg_registro_json"));

            entity.Property(e => e.DtCreationDt).HasDefaultValueSql("(getdate())");
        });

        modelBuilder.Entity<FacRegistroJsonTipo>(entity =>
        {
            entity.ToTable("facRegistroJsonTipo", tb => tb.HasTrigger("trg_json_tipo"));

            entity.Property(e => e.DtCreationDt).HasDefaultValueSql("(getdate())");
        });

        modelBuilder.Entity<FacturaElec>(entity =>
        {
            entity.ToView("FacturaElec");
        });

        modelBuilder.Entity<FacturaElecAnulacion>(entity =>
        {
            entity.ToView("FacturaElecAnulacion");
        });

        modelBuilder.Entity<FacturaElecConsulta>(entity =>
        {
            entity.ToView("FacturaElecConsulta");
        });

        modelBuilder.Entity<FacturaElecDetalle>(entity =>
        {
            entity.ToView("FacturaElecDetalle");
        });

        modelBuilder.Entity<FacturaElecMetadato>(entity =>
        {
            entity.ToView("FacturaElecMetadato");
        });

        modelBuilder.Entity<FacturaLibelulaEvento>(entity =>
        {
            entity.ToView("FacturaLibelulaEvento");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
