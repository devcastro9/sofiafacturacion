# Docker NET9
FROM mcr.microsoft.com/dotnet/aspnet:9.0 AS base
ENV TZ=America/La_Paz
ENV ASPNETCORE_URLS=http://+:5000
ENV ASPNETCORE_ENVIRONMENT=Production
WORKDIR /app
EXPOSE 5000

FROM mcr.microsoft.com/dotnet/sdk:9.0 AS build
WORKDIR /src
COPY ["SofiaFacturacion.csproj", "."]
RUN dotnet restore "./SofiaFacturacion.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "SofiaFacturacion.csproj" -c Release -o /app/build --nologo

FROM build AS publish
RUN dotnet publish "SofiaFacturacion.csproj" -c Release -o /app/publish /p:UseAppHost=false --nologo

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "SofiaFacturacion.dll"]