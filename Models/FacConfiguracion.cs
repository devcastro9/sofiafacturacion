﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SofiaFacturacion.Models;

[Table("facConfiguracion")]
public partial class FacConfiguracion
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int FacConfigId { get; set; }

    public int EmpresaId { get; set; }

    [StringLength(36)]
    [Unicode(false)]
    public string? AppKey { get; set; }

    [StringLength(64)]
    [Unicode(false)]
    public string? CanalCaja { get; set; }

    [StringLength(394)]
    [Unicode(false)]
    public string? TokenDelegado { get; set; }

    [StringLength(100)]
    [Unicode(false)]
    public string? CodigoSistema { get; set; }

    [StringLength(150)]
    [Unicode(false)]
    public string? CodigoUnicoInicioSesion { get; set; }

    [StringLength(100)]
    [Unicode(false)]
    public string? CallbackUrlBase { get; set; }

    public bool HabilitadoFacturacion { get; set; }

    public bool FacturacionBackgroundActivo { get; set; }

    public bool Estado { get; set; }

    public bool ValidarEmail { get; set; }

    [Column("ValidarNIT")]
    public bool ValidarNit { get; set; }

    [Column("sLastUpdate_id")]
    [StringLength(50)]
    [Unicode(false)]
    public string? SLastUpdateId { get; set; }

    [Column("dtLastUpdate_dt", TypeName = "datetime")]
    public DateTime? DtLastUpdateDt { get; set; }

    [Column("iConcurrency_id")]
    public short IConcurrencyId { get; set; }
}
