﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SofiaFacturacion.Models;

[Keyless]
public partial class FacturaElecAnulacion
{
    [StringLength(36)]
    [Unicode(false)]
    public string? AppKey { get; set; }

    public long IdFactura { get; set; }

    [Column("monto", TypeName = "decimal(18, 2)")]
    public decimal? Monto { get; set; }

    [Column("motivo")]
    [StringLength(250)]
    [Unicode(false)]
    public string? Motivo { get; set; }

    [Column("valido_anulacion")]
    public bool? ValidoAnulacion { get; set; }
}
