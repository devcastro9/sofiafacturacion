﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace SofiaFacturacion.Models;

[Keyless]
public partial class FacturaElecConsulta
{
    public long IdFactura { get; set; }

    [StringLength(36)]
    [Unicode(false)]
    public string? AppKey { get; set; }
}
