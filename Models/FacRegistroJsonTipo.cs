﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SofiaFacturacion.Models;

[Table("facRegistroJsonTipo")]
public partial class FacRegistroJsonTipo
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int TipoJsonId { get; set; }

    [StringLength(50)]
    public string? Descripcion { get; set; }

    [StringLength(20)]
    public string? Metodo { get; set; }

    [Column("dtCreation_dt", TypeName = "datetime")]
    public DateTime? DtCreationDt { get; set; }
}
