﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SofiaFacturacion.Models;

[Table("facRegistroEvento")]
public partial class FacRegistroEvento
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public long RegistroId { get; set; }

    public long IdFactura { get; set; }

    public int Error { get; set; }

    public int Existente { get; set; }

    [StringLength(1000)]
    [Unicode(false)]
    public string? Mensaje { get; set; }

    [StringLength(36)]
    [Unicode(false)]
    public string? IdTransaccion { get; set; }

    [StringLength(200)]
    [Unicode(false)]
    public string? UrlPasarelaPagos { get; set; }

    [Column("dtCreation_dt", TypeName = "datetime")]
    public DateTime? DtCreationDt { get; set; }
}
