﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SofiaFacturacion.Models;

[Table("facDominioEmail")]
public partial class FacDominioEmail
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int IdDominio { get; set; }

    [StringLength(150)]
    [Unicode(false)]
    public string DominioIgnorado { get; set; } = null!;

    public bool? Habilitado { get; set; }
}
