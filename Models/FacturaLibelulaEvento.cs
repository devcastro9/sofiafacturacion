﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SofiaFacturacion.Models;

[Keyless]
public partial class FacturaLibelulaEvento
{
    [Column("ID")]
    public long Id { get; set; }

    [Column(TypeName = "datetime")]
    public DateTime? Fecha { get; set; }

    public int? CodCliente { get; set; }

    [StringLength(150)]
    [Unicode(false)]
    public string? Cliente { get; set; }

    [StringLength(250)]
    [Unicode(false)]
    public string? Mensaje { get; set; }

    [Column(TypeName = "decimal(18, 2)")]
    public decimal? Monto { get; set; }

    [StringLength(20)]
    [Unicode(false)]
    public string? SolicitadoPor { get; set; }

    [StringLength(20)]
    [Unicode(false)]
    public string? AprobadoPor { get; set; }

    [Column(TypeName = "datetime")]
    public DateTime? FechaApr { get; set; }

    [StringLength(100)]
    [Unicode(false)]
    public string? Descripcion { get; set; }
}
