﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SofiaFacturacion.Models;

[Table("facKey")]
public partial class FacKey
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int IdKey { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? PorFuncion { get; set; }

    [StringLength(150)]
    [Unicode(false)]
    public string? Appkey { get; set; }

    public bool Estado { get; set; }
}
