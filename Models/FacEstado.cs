﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SofiaFacturacion.Models;

[Table("facEstado")]
public partial class FacEstado
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int EstadoFacId { get; set; }

    [StringLength(100)]
    [Unicode(false)]
    public string? Descripcion { get; set; }

    public int? TipoFacId { get; set; }

    public bool? Habilitado { get; set; }

    [Column("sLastUpdate_id")]
    [StringLength(50)]
    [Unicode(false)]
    public string? SLastUpdateId { get; set; }

    [Column("dtLastUpdate_dt", TypeName = "datetime")]
    public DateTime? DtLastUpdateDt { get; set; }

    [Column("iConcurrency_id")]
    public short? IConcurrencyId { get; set; }

    public bool? EsModificable { get; set; }

    [ForeignKey("TipoFacId")]
    [InverseProperty("FacEstados")]
    public virtual FacEstadoTipo? TipoFac { get; set; }
}
