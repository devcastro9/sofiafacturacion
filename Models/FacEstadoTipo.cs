﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SofiaFacturacion.Models;

[Table("facEstadoTipo")]
public partial class FacEstadoTipo
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int TipoFacId { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? Descripcion { get; set; }

    public bool? EsCola { get; set; }

    [Column("sLastUpdate_id")]
    [StringLength(50)]
    [Unicode(false)]
    public string? SLastUpdateId { get; set; }

    [Column("dtLastUpdate_dt", TypeName = "datetime")]
    public DateTime? DtLastUpdateDt { get; set; }

    [Column("iConcurrency_id")]
    public short? IConcurrencyId { get; set; }

    [InverseProperty("TipoFac")]
    public virtual ICollection<FacEstado> FacEstados { get; set; } = new List<FacEstado>();
}
