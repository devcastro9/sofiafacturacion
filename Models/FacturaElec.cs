﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SofiaFacturacion.Models;

[Keyless]
public partial class FacturaElec
{
    [StringLength(36)]
    [Unicode(false)]
    public string? AppKey { get; set; }

    public long IdFactura { get; set; }

    [Column("email")]
    [StringLength(80)]
    [Unicode(false)]
    public string? Email { get; set; }

    [Column("curl")]
    [StringLength(100)]
    [Unicode(false)]
    public string? Curl { get; set; }

    [Column("razon_social")]
    [StringLength(150)]
    [Unicode(false)]
    public string? RazonSocial { get; set; }

    [Column("nit")]
    [StringLength(20)]
    [Unicode(false)]
    public string? Nit { get; set; }

    [Column("tipo_doc")]
    [StringLength(3)]
    [Unicode(false)]
    public string? TipoDoc { get; set; }

    [Column("codigo_cliente")]
    public int? CodigoCliente { get; set; }

    [Column("factura_nota")]
    [StringLength(250)]
    [Unicode(false)]
    public string? FacturaNota { get; set; }

    [Column("tipo_factura")]
    [StringLength(1)]
    [Unicode(false)]
    public string TipoFactura { get; set; } = null!;

    [Column("emite_factura")]
    public int EmiteFactura { get; set; }

    [Column("cambio_oficial", TypeName = "decimal(18, 2)")]
    public decimal? CambioOficial { get; set; }

    [Column("tipo_moneda")]
    [StringLength(4)]
    [Unicode(false)]
    public string? TipoMoneda { get; set; }

    [Column("canal_caja")]
    [StringLength(64)]
    [Unicode(false)]
    public string? CanalCaja { get; set; }

    [Column("canal_caja_sucursal")]
    [StringLength(2)]
    [Unicode(false)]
    public string? CanalCajaSucursal { get; set; }

    [Column("canal_caja_usuario")]
    [StringLength(30)]
    [Unicode(false)]
    public string? CanalCajaUsuario { get; set; }

    [Column("total", TypeName = "decimal(18, 2)")]
    public decimal? Total { get; set; }

    public bool ValidarEmail { get; set; }

    [Column("ValidarNIT")]
    public bool ValidarNit { get; set; }

    [Column("MetodoPago")]
    [StringLength(100)]
    public string? MetodoPago { get; set; }
}
