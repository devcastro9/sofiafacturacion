﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace SofiaFacturacion.Models;

[Keyless]
public partial class FacturaElecMetadato
{
    [StringLength(8)]
    [Unicode(false)]
    public string Nombre { get; set; } = null!;

    [StringLength(200)]
    [Unicode(false)]
    public string? Dato { get; set; }

    public long IdFactura { get; set; }
}
