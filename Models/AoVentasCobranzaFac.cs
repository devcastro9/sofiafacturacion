﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SofiaFacturacion.Models;

[Table("ao_ventas_cobranza_fac")]
public partial class AoVentasCobranzaFac
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public long IdFactura { get; set; }

    [Column("tipo_bien_servicio")]
    public int? TipoBienServicio { get; set; }

    [Column("ges_gestion")]
    [StringLength(4)]
    [Unicode(false)]
    public string? GesGestion { get; set; }

    [Column("dosifica_autorizacion")]
    [StringLength(100)]
    [Unicode(false)]
    public string? DosificaAutorizacion { get; set; }

    [Column("nro_factura")]
    public long? NroFactura { get; set; }

    [Column("doc_codigo_fac")]
    [StringLength(6)]
    [Unicode(false)]
    public string? DocCodigoFac { get; set; }

    [Column("fecha_fac", TypeName = "datetime")]
    public DateTime? FechaFac { get; set; }

    [Column("venta_codigo")]
    public long? VentaCodigo { get; set; }

    [Column("edif_codigo")]
    [StringLength(21)]
    [Unicode(false)]
    public string? EdifCodigo { get; set; }

    [Column("edif_codigo_corto")]
    public int? EdifCodigoCorto { get; set; }

    [Column("tipodoc_codigo")]
    [StringLength(3)]
    [Unicode(false)]
    public string? TipodocCodigo { get; set; }

    [Column("tipodoc_sin")]
    [StringLength(3)]
    [Unicode(false)]
    public string? TipodocSin { get; set; }

    [Column("beneficiario_codigo_fac")]
    [StringLength(20)]
    [Unicode(false)]
    public string? BeneficiarioCodigoFac { get; set; }

    [Column("beneficiario_nit")]
    [StringLength(20)]
    [Unicode(false)]
    public string? BeneficiarioNit { get; set; }

    [Column("glosa_Descripcion")]
    [StringLength(250)]
    [Unicode(false)]
    public string? GlosaDescripcion { get; set; }

    [Column("beneficiario_RazonSocial")]
    [StringLength(150)]
    [Unicode(false)]
    public string? BeneficiarioRazonSocial { get; set; }

    [Column("beneficiario_email")]
    [StringLength(80)]
    [Unicode(false)]
    public string? BeneficiarioEmail { get; set; }

    [Column("email_cobrador")]
    [StringLength(80)]
    [Unicode(false)]
    public string? EmailCobrador { get; set; }

    [Column("nro_dui")]
    [StringLength(30)]
    [Unicode(false)]
    public string? NroDui { get; set; }

    [Column("total_bs", TypeName = "decimal(18, 2)")]
    public decimal? TotalBs { get; set; }

    [Column("total_dol", TypeName = "decimal(18, 2)")]
    public decimal? TotalDol { get; set; }

    [Column("cambio_oficial", TypeName = "decimal(18, 2)")]
    public decimal? CambioOficial { get; set; }

    [Column("Importe_ICE", TypeName = "decimal(18, 2)")]
    public decimal? ImporteIce { get; set; }

    [Column("Exportaciones_Exentas", TypeName = "decimal(18, 2)")]
    public decimal? ExportacionesExentas { get; set; }

    [Column("Ventas_tasa_0", TypeName = "decimal(18, 2)")]
    public decimal? VentasTasa0 { get; set; }

    [Column("Subtotal_ICE", TypeName = "decimal(18, 2)")]
    public decimal? SubtotalIce { get; set; }

    [Column("Descuentos_Bonos", TypeName = "decimal(18, 2)")]
    public decimal? DescuentosBonos { get; set; }

    [Column("Importe_Base_Debito_Fiscal", TypeName = "decimal(18, 2)")]
    public decimal? ImporteBaseDebitoFiscal { get; set; }

    [Column("factura_87_bs", TypeName = "decimal(18, 2)")]
    public decimal? Factura87Bs { get; set; }

    [Column("factura_87_dol", TypeName = "decimal(18, 2)")]
    public decimal? Factura87Dol { get; set; }

    [Column("debito_fiscal_13_bs", TypeName = "decimal(18, 2)")]
    public decimal? DebitoFiscal13Bs { get; set; }

    [Column("debito_fiscal_13_dol", TypeName = "decimal(18, 2)")]
    public decimal? DebitoFiscal13Dol { get; set; }

    [Column("codigo_control")]
    [StringLength(30)]
    [Unicode(false)]
    public string? CodigoControl { get; set; }

    [Column("literal")]
    [StringLength(200)]
    [Unicode(false)]
    public string? Literal { get; set; }

    [Column("clasif_codigo")]
    [StringLength(3)]
    [Unicode(false)]
    public string? ClasifCodigo { get; set; }

    [Column("doc_codigo")]
    [StringLength(6)]
    [Unicode(false)]
    public string? DocCodigo { get; set; }

    [Column("doc_numero")]
    public int? DocNumero { get; set; }

    [Column("factura_impresa")]
    [StringLength(1)]
    [Unicode(false)]
    public string? FacturaImpresa { get; set; }

    [Column("tipo_moneda")]
    [StringLength(4)]
    [Unicode(false)]
    public string? TipoMoneda { get; set; }

    [Column("cta_codigo")]
    [StringLength(40)]
    [Unicode(false)]
    public string? CtaCodigo { get; set; }

    [Column("cta_codigo2")]
    [StringLength(40)]
    [Unicode(false)]
    public string? CtaCodigo2 { get; set; }

    [Column("correl_contab")]
    public double? CorrelContab { get; set; }

    [StringLength(4)]
    [Unicode(false)]
    public string? Gestion { get; set; }

    public int? Mes { get; set; }

    [Column("codigo_empresa")]
    public int? CodigoEmpresa { get; set; }

    [Column("depto_codigo")]
    [StringLength(2)]
    [Unicode(false)]
    public string? DeptoCodigo { get; set; }

    [Column("unidad_codigo")]
    [StringLength(15)]
    [Unicode(false)]
    public string? UnidadCodigo { get; set; }

    [Column("etapa_codigo")]
    [StringLength(9)]
    [Unicode(false)]
    public string? EtapaCodigo { get; set; }

    [Column("estado_fac")]
    [StringLength(3)]
    [Unicode(false)]
    public string? EstadoFac { get; set; }

    [Column("estado_codigo_fac")]
    [StringLength(3)]
    [Unicode(false)]
    public string? EstadoCodigoFac { get; set; }

    [Column("estado_codigo")]
    [StringLength(3)]
    [Unicode(false)]
    public string? EstadoCodigo { get; set; }

    [Column("estado_cobrado")]
    [StringLength(3)]
    [Unicode(false)]
    public string? EstadoCobrado { get; set; }

    [Column("estado_contab")]
    [StringLength(3)]
    [Unicode(false)]
    public string? EstadoContab { get; set; }

    [Column("usr_codigo")]
    [StringLength(20)]
    [Unicode(false)]
    public string? UsrCodigo { get; set; }

    [Column("usr_codigo_apr")]
    [StringLength(20)]
    [Unicode(false)]
    public string? UsrCodigoApr { get; set; }

    [Column("usr_codigo_anl")]
    [StringLength(20)]
    [Unicode(false)]
    public string? UsrCodigoAnl { get; set; }

    [Column("fecha_registro", TypeName = "datetime")]
    public DateTime? FechaRegistro { get; set; }

    [Column("fecha_aprueba", TypeName = "datetime")]
    public DateTime? FechaAprueba { get; set; }

    [Column("fecha_anula", TypeName = "datetime")]
    public DateTime? FechaAnula { get; set; }

    [Column("fecha_envio", TypeName = "datetime")]
    public DateTime? FechaEnvio { get; set; }

    [Column("hora_registro")]
    [StringLength(8)]
    [Unicode(false)]
    public string? HoraRegistro { get; set; }

    [Column("estado_enviado")]
    public int? EstadoEnviado { get; set; }

    [Column("solicitud_tipo")]
    public int? SolicitudTipo { get; set; }

    [Column("trans_codigo")]
    [StringLength(2)]
    [Unicode(false)]
    public string? TransCodigo { get; set; }

    [StringLength(250)]
    [Unicode(false)]
    public string? JustificaAnulacionFac { get; set; }

    [Column("fecha_fac_sin", TypeName = "datetime")]
    public DateTime? FechaFacSin { get; set; }

    public int EstadoFacId { get; set; }

    [StringLength(96)]
    [Unicode(false)]
    public string? Identificador { get; set; }

    [StringLength(70)]
    [Unicode(false)]
    public string? Cufd { get; set; }

    [StringLength(200)]
    [Unicode(false)]
    public string? UrlTodotix { get; set; }

    [StringLength(200)]
    [Unicode(false)]
    public string? UrlSinSfe { get; set; }
}
