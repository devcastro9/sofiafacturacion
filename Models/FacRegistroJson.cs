﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SofiaFacturacion.Models;

[Table("facRegistroJson")]
public partial class FacRegistroJson
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public long RegistroJsonId { get; set; }

    public int? TipoJsonId { get; set; }

    public string? JsonRequest { get; set; }

    public string? JsonResponse { get; set; }

    [Column("dtCreation_dt", TypeName = "datetime")]
    public DateTime? DtCreationDt { get; set; }
}
