﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SofiaFacturacion.Data;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SofiaFacturacion.HostedServices;

public class ProcReAdicion : IHostedService, IDisposable
{
    private readonly IServiceScopeFactory _scopeFactory;
    private readonly ILogger<ProcReAdicion> _logger;
    private Timer? _timer = null;
    public ProcReAdicion(IServiceScopeFactory scopeFactory, ILogger<ProcReAdicion> logger)
    {
        _scopeFactory = scopeFactory;
        _logger = logger;
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("Hosted service facturador iniciando {DateTime.Now.ToLongTimeString()}", DateTime.Now.ToLongTimeString());
        _timer = new Timer(ReAdicionar, null, TimeSpan.FromSeconds(200), TimeSpan.FromMinutes(3));
        return Task.CompletedTask;
    }

    private async void ReAdicionar(object? state)
    {
        using IServiceScope ambito = _scopeFactory.CreateScope();
        AppDbContext _context = ambito.ServiceProvider.GetRequiredService<AppDbContext>();
        // Poner todos los procedimientos almacenados pesados
        _logger.LogInformation("Re-encolamiento iniciado {DateTime.Now.ToLongTimeString()}", DateTime.Now.ToLongTimeString());
        try
        {
            _ = await _context.Database.ExecuteSqlRawAsync("EXECUTE [dbo].[facProcedimientoReAdicion]");
        }
        catch (Exception ex)
        {
            _logger.LogError("Error re-encolamiento: {ex.Message} {DateTime.Now.ToLongTimeString()}", ex.Message, DateTime.Now.ToLongTimeString());
        }
        _logger.LogInformation("Re-encolamiento finalizado {DateTime.Now.ToLongTimeString()}", DateTime.Now.ToLongTimeString());
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("Re-encolamiento esta deteniendose {DateTime.Now.ToLongTimeString()}", DateTime.Now.ToLongTimeString());
        _timer?.Change(Timeout.Infinite, 0);
        return Task.CompletedTask;
    }

    public void Dispose()
    {
        _timer?.Dispose();
        GC.SuppressFinalize(this);
    }
}
