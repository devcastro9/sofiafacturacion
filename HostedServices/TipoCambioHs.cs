﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SofiaFacturacion.Data;
using SofiaFacturacion.Services;
using SofiaFacturacion.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SofiaFacturacion.HostedServices;

public class TipoCambioHs : IHostedService, IDisposable
{
    private readonly IServiceScopeFactory _scopeFactory;

    private readonly BcbApi _bcbApi;

    private readonly ApiLayer _apiLayer;

    private readonly ILogger<TipoCambioHs> _logger;

    private Timer? _timer = null;

    public TipoCambioHs(IServiceScopeFactory scopeFactory, BcbApi bcbApi, ApiLayer apiLayer, ILogger<TipoCambioHs> logger)
    {
        _scopeFactory = scopeFactory;
        _bcbApi = bcbApi;
        _apiLayer = apiLayer;
        _logger = logger;
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("Hosted service Tipo de Cambio iniciando {DateTime.Now.ToLongTimeString()}", DateTime.Now.ToLongTimeString());
        _timer = new Timer(ProcesarTC, null, TimeSpan.FromHours(1), TimeSpan.FromHours(1));
        return Task.CompletedTask;
    }

    private async void ProcesarTC(object? state)
    {
        using IServiceScope ambito = _scopeFactory.CreateScope();
        AppDbContext _context = ambito.ServiceProvider.GetRequiredService<AppDbContext>();
        // Poner todos los procedimientos almacenados pesados
        _logger.LogInformation("Tipo de cambio iniciado {DateTime.Now.ToLongTimeString()}", DateTime.Now.ToLongTimeString());
        try
        {
            // Diario (Cada 6am)
            if (DateTime.Now.Hour == 6)
            {
                string Key = await (from c in _context.FacKeys where c.IdKey == 2 select c.Appkey).AsNoTracking().FirstAsync();
                IList<BcbUfvRes>? ValorUfv = await _bcbApi.GetUfv(DateTime.Now, DateTime.Now);
                ApiLayerRes? ValorEur = await _apiLayer.ConvertValue("BOB", "EUR", 1, DateTime.Now, Key);
                ApiLayerRes? ValorRmb = await _apiLayer.ConvertValue("BOB", "CNY", 1, DateTime.Now, Key);
                ApiLayerRes? ValorBrl = await _apiLayer.ConvertValue("BOB", "BRL", 1, DateTime.Now, Key);
                if (ValorUfv is null || ValorEur is null || ValorRmb is null || ValorBrl is null)
                {
                    return;
                }
                SqlParameter fecha = new("@Fecha", SqlDbType.Date)
                {
                    Value = DateTime.Now
                };
                SqlParameter Ufv = new("@ValorUfv", SqlDbType.Decimal)
                {
                    Precision = 18,
                    Scale = 5,
                    Value = (from u in ValorUfv select u.Ufv).FirstOrDefault()
                };
                SqlParameter Euro = new("@ValorEur", SqlDbType.Decimal)
                {
                    Precision = 18,
                    Scale = 3,
                    Value = ValorEur.Resultado
                };
                SqlParameter Yuan = new("@ValorRmb", SqlDbType.Decimal)
                {
                    Precision = 18,
                    Scale = 3,
                    Value = ValorRmb.Resultado
                };
                SqlParameter Real = new("@ValorBrl", SqlDbType.Decimal)
                {
                    Precision = 18,
                    Scale = 3,
                    Value = ValorBrl.Resultado
                };
                _ = await _context.Database.ExecuteSqlRawAsync("EXECUTE [dbo].[cargar_tc_ufv] @Fecha, @ValorUfv, @ValorEur, @ValorRmb, @ValorBrl", fecha, Ufv, Euro, Yuan, Real);
            }
            // Cada 10 de mes (10 de mes a las 6am)
            if (DateTime.Now.Hour == 6 && DateTime.Now.Day == 10)
            {
                IList<BcbUfvRes>? ValorUfvMes = await _bcbApi.GetUfv(DateTime.Now.AddDays(1), DateTime.Now.AddMonths(1));
                if (ValorUfvMes is null)
                {
                    return;
                }
                foreach (BcbUfvRes item in ValorUfvMes)
                {
                    SqlParameter fechaM = new("@Fecha", SqlDbType.Date)
                    {
                        Value = item.Fecha
                    };
                    SqlParameter UfvM = new("@ValorUfv", SqlDbType.Decimal)
                    {
                        Precision = 18,
                        Scale = 5,
                        Value = item.Ufv
                    };
                    _ = await _context.Database.ExecuteSqlRawAsync("EXECUTE [dbo].[cargar_tc_ufv] @Fecha, @ValorUfv, @ValorEur, @ValorRmb, @ValorBrl", fechaM, UfvM, 8, 1, 1);
                }
            }
        }
        catch (Exception ex)
        {
            _logger.LogError("Error Tipo de Cambio: {ex.Message} {DateTime.Now.ToLongTimeString()}", ex.Message, DateTime.Now.ToLongTimeString());
        }
        _logger.LogInformation("Tipo de cambio finalizado {DateTime.Now.ToLongTimeString()}", DateTime.Now.ToLongTimeString());
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("Hosted Service Tipo de Cambio deteniendose.");
        _timer?.Change(Timeout.Infinite, 0);
        return Task.CompletedTask;
    }

    public void Dispose()
    {
        _timer?.Dispose();
        GC.SuppressFinalize(this);
    }
}
