﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SofiaFacturacion.Data;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SofiaFacturacion.HostedServices;

public class ProcGeneral : IHostedService, IDisposable
{
    private readonly IServiceScopeFactory _scopeFactory;
    private readonly ILogger<ProcGeneral> _logger;
    private Timer? _timer = null;
    public ProcGeneral(IServiceScopeFactory scopeFactory, ILogger<ProcGeneral> logger)
    {
        _scopeFactory = scopeFactory;
        _logger = logger;
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("Hosted service ProcGeneral iniciando {DateTime.Now.ToLongTimeString()}", DateTime.Now.ToLongTimeString());
        _timer = new Timer(Procesar, null, TimeSpan.FromSeconds(0), TimeSpan.FromMinutes(1));
        return Task.CompletedTask;
    }

    private async void Procesar(object? state)
    {
        using IServiceScope ambito = _scopeFactory.CreateScope();
        AppDbContext _context = ambito.ServiceProvider.GetRequiredService<AppDbContext>();
        // Poner todos los procedimientos almacenados pesados
        _logger.LogInformation("Proceso gral iniciada {DateTime.Now.ToLongTimeString()}", DateTime.Now.ToLongTimeString());
        try
        {
            _ = await _context.Database.ExecuteSqlRawAsync("EXECUTE [dbo].[facProcedimientoGeneral]");
        }
        catch (Exception ex)
        {
            _logger.LogError("Error ProcGral: {ex.Message} {DateTime.Now.ToLongTimeString()}", ex.Message, DateTime.Now.ToLongTimeString());
        }
        _logger.LogInformation("Proceso gral finalizada {DateTime.Now.ToLongTimeString()}", DateTime.Now.ToLongTimeString());
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("Hosted Service ProcGeneral deteniendose.");
        _timer?.Change(Timeout.Infinite, 0);
        return Task.CompletedTask;
    }

    public void Dispose()
    {
        _timer?.Dispose();
        GC.SuppressFinalize(this);
    }
}
