﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MimeKit;
using SofiaFacturacion.Data;
using SofiaFacturacion.Models;
using SofiaFacturacion.Services;
using SofiaFacturacion.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace SofiaFacturacion.HostedServices;

public class Anulador : IHostedService, IDisposable
{
    private readonly IServiceScopeFactory _scopeFactory;
    private readonly LibelulaApi _libelulaApi;
    private readonly ViewSiatFacEstado _viewSiatFac;
    private readonly ITestInternet _internet;
    private readonly ILogger<Anulador> _logger;
    private Timer? _timer = null;
    public Anulador(IServiceScopeFactory scopeFactory, LibelulaApi libelulaApi, ViewSiatFacEstado viewSiatFac, ITestInternet testInternet, ILogger<Anulador> logger)
    {
        _scopeFactory = scopeFactory;
        _libelulaApi = libelulaApi;
        _viewSiatFac = viewSiatFac;
        _internet = testInternet;
        _logger = logger;
    }
    public Task StartAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("Hosted service facturador iniciando {DateTime.Now.ToLongTimeString()}", DateTime.Now.ToLongTimeString());
        _timer = new Timer(Anular, null, TimeSpan.FromSeconds(330), TimeSpan.FromMinutes(5));
        return Task.CompletedTask;
    }

    private async void Anular(object? state)
    {
        int contador = 0;
        int numeroLote = 5;
        //ParallelOptions options = new() { MaxDegreeOfParallelism = Environment.ProcessorCount };
        using IServiceScope ambito = _scopeFactory.CreateScope();
        AppDbContext _context = ambito.ServiceProvider.GetRequiredService<AppDbContext>();
        ISiatFacturacion _siat = ambito.ServiceProvider.GetRequiredService<ISiatFacturacion>();
        IEmailSmtp _email = ambito.ServiceProvider.GetRequiredService<IEmailSmtp>();
        // Internet
        bool EsInternetOnline = await _internet.ExisteInternet();
        if (!EsInternetOnline)
        {
            _logger.LogWarning("No existe una conexion activa a internet");
            return;
        }
        // SIAT
        string? TokenSiat = await _context.FacConfiguraciones.AsNoTracking().Where(m => m.Estado).Select(m => m.TokenDelegado).FirstOrDefaultAsync();
        bool EsSiatOnline = await _siat.VerificarComunicacionSiat(TokenSiat);
        if (!EsSiatOnline)
        {
            _logger.LogWarning("Los servicios de Impuestos Nacionales no estan activos");
            return;
        }
        // Libelula
        string? Appkey = await _context.FacConfiguraciones.AsNoTracking().Where(m => m.Estado).Select(m => m.AppKey).FirstOrDefaultAsync();
        bool EsLibelulaOnline = await _libelulaApi.EsOnline(Appkey);
        if (!EsLibelulaOnline)
        {
            _logger.LogWarning("Los servicios de Libelula/Todotix no estan activos");
            return;
        }
        // Consultar datos
        _logger.LogInformation("Anulacion iniciada {DateTime.Now.ToLongTimeString()}", DateTime.Now.ToLongTimeString());
        List<FacturaElecAnulacion> FacAnulador = await _context.FacturaElecAnulaciones.AsNoTracking().Where(m => m.ValidoAnulacion == true).ToListAsync();
        foreach (FacturaElecAnulacion item in FacAnulador)
        {
            AnulaPagoReq request = new()
            {
                IdFactura = item.IdFactura,
                AppKey = item.AppKey ?? "",
                Monto = item.Monto ?? 0,
                Motivo = item.Motivo
            };
            // Metodo POST
            AnulaPagoRes? response = await _libelulaApi.AnularFactura(request);
            contador++;
            // Guardado de logs de peticiones
            FacRegistroJson jsonLogs = new()
            {
                TipoJsonId = 2,
                JsonRequest = JsonSerializer.Serialize(request),
                JsonResponse = JsonSerializer.Serialize(response)
            };
            _ = await _context.FacRegistroJsons.AddAsync(jsonLogs);
            // Data de factura
            AoVentasCobranzaFac? datosAnulacion = await _context.AoVentasCobranzaFacs.FindAsync(item.IdFactura);
            // Caso no posible, el IdFactura viene de la base de datos
            if (datosAnulacion is null)
            {
                continue;
            }
            // Respuesta nula
            if (response is null || response.Mensaje is null)
            {
                FacRegistroEvento eventoNull = new()
                {
                    IdFactura = item.IdFactura,
                    Error = 1,
                    Existente = 0,
                    Mensaje = "Respuesta nula de Libelula",
                    IdTransaccion = "",
                    UrlPasarelaPagos = "ANULACION"
                };
                _ = await _context.FacRegistroEventos.AddAsync(eventoNull);
                _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {item.IdFactura}, 20");
                continue;
            }
            FacRegistroEvento evento = new()
            {
                IdFactura = item.IdFactura,
                Error = response.Estado ? 0 : 1,
                Existente = 0,
                Mensaje = response.Mensaje,
                IdTransaccion = "",
                UrlPasarelaPagos = "ANULACION"
            };
            _ = await _context.FacRegistroEventos.AddAsync(evento);
            // Importes distintos
            if (response.Mensaje.Contains("no coincide con el importe total"))
            {
                MimeMessage emailMonto = _email.AnulacionFacturaEmail("Se detecto un problema con diferencia en los montos.", item.IdFactura, datosAnulacion.TipoMoneda == "BOB" ? datosAnulacion.TotalBs ?? 0 : datosAnulacion.TotalDol ?? 0, datosAnulacion.EdifCodigoCorto.ToString() ?? "", datosAnulacion.BeneficiarioRazonSocial ?? "", datosAnulacion.UrlSinSfe ?? "");
                _ = await _email.EnviarEmail(emailMonto);
                _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {item.IdFactura}, 21");
                continue;
            }
            // Validacion en SIN
            if (datosAnulacion.UrlSinSfe is null)
            {
                _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {item.IdFactura}, 22");
                continue;
            }
            bool? responseSIN = await _viewSiatFac.EsFacturaAnulada(datosAnulacion.UrlSinSfe);
            if (responseSIN is null)
            {
                _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {item.IdFactura}, 22");
                continue;
            }
            int estado_anulacion = 0;
            estado_anulacion = response.Estado switch
            {
                true => (bool)responseSIN ? 9 : 19,
                _ => 20
            };
            _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {item.IdFactura}, {estado_anulacion}");
            if (estado_anulacion == 19)
            {
                MimeMessage emailMonto = _email.AnulacionFacturaEmail("La factura esta anulada en Libelula pero no en Impuestos Nacionales, por favor verifique.", item.IdFactura, datosAnulacion.TipoMoneda == "BOB" ? datosAnulacion.TotalBs ?? 0 : datosAnulacion.TotalDol ?? 0, datosAnulacion.EdifCodigoCorto.ToString() ?? "", datosAnulacion.BeneficiarioRazonSocial ?? "", datosAnulacion.UrlSinSfe ?? "");
                _ = await _email.EnviarEmail(emailMonto);
            }
            if (estado_anulacion == 20)
            {
                MimeMessage emailMonto = _email.AnulacionFacturaEmail("La factura tuvo un fallo al anular.", item.IdFactura, datosAnulacion.TipoMoneda == "BOB" ? datosAnulacion.TotalBs ?? 0 : datosAnulacion.TotalDol ?? 0, datosAnulacion.EdifCodigoCorto.ToString() ?? "", datosAnulacion.BeneficiarioRazonSocial ?? "", datosAnulacion.UrlSinSfe ?? "");
                _ = await _email.EnviarEmail(emailMonto);
            }
            // Guardado de cambios por lote
            if (contador % numeroLote == 0)
            {
                try
                {
                    _ = await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException dbError)
                {
                    _logger.LogError("Error facturacion: {db.Message} {DateTime.Now.ToLongTimeString()}", dbError.Message, DateTime.Now.ToLongTimeString());
                    //throw;
                }
            }
        }
        // Guardado de cambios fuera del lote
        try
        {
            _ = await _context.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException dbError)
        {
            _logger.LogError("Error facturacion: {db.Message} {DateTime.Now.ToLongTimeString()}", dbError.Message, DateTime.Now.ToLongTimeString());
            //throw;
        }
        _logger.LogInformation("Anulacion finalizada {DateTime.Now.ToLongTimeString()}", DateTime.Now.ToLongTimeString());
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("Hosted Service facturador deteniendose.");
        _timer?.Change(Timeout.Infinite, 0);
        return Task.CompletedTask;
    }

    public void Dispose()
    {
        _timer?.Dispose();
        GC.SuppressFinalize(this);
    }
}
