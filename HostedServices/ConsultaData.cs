﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SofiaFacturacion.Data;
using SofiaFacturacion.Models;
using SofiaFacturacion.Services;
using SofiaFacturacion.Utility;
using SofiaFacturacion.ViewModels;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace SofiaFacturacion.HostedServices;

public class ConsultaData : IHostedService, IDisposable
{
    private readonly IServiceScopeFactory _scopeFactory;
    private readonly LibelulaApi _libelulaApi;
    private readonly ITestInternet _internet;
    private readonly ILogger<ConsultaData> _logger;
    private Timer? _timer = null;
    public ConsultaData(IServiceScopeFactory scopeFactory, LibelulaApi libelulaApi, ITestInternet internet, ILogger<ConsultaData> logger)
    {
        _scopeFactory = scopeFactory;
        _libelulaApi = libelulaApi;
        _internet = internet;
        _logger = logger;
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("HostedService: Consulta de datos de facturas iniciando {DateTime.Now.ToLongTimeString()}", DateTime.Now.ToLongTimeString());
        _timer = new Timer(Consultar, null, TimeSpan.FromSeconds(460), TimeSpan.FromMinutes(7));
        return Task.CompletedTask;
    }

    private async void Consultar(object? state)
    {
        int contador = 0;
        int numeroLote = 5;
        ParallelOptions options = new() { MaxDegreeOfParallelism = Environment.ProcessorCount };
        using IServiceScope ambito = _scopeFactory.CreateScope();
        AppDbContext _context = ambito.ServiceProvider.GetRequiredService<AppDbContext>();
        ISiatFacturacion _siat = ambito.ServiceProvider.GetRequiredService<ISiatFacturacion>();
        // Internet
        bool EsInternetOnline = await _internet.ExisteInternet();
        if (!EsInternetOnline)
        {
            _logger.LogWarning("No existe una conexion activa a internet");
            return;
        }
        // SIAT
        string? TokenSiat = await _context.FacConfiguraciones.AsNoTracking().Where(m => m.Estado).Select(m => m.TokenDelegado).FirstOrDefaultAsync();
        bool EsSiatOnline = await _siat.VerificarComunicacionSiat(TokenSiat);
        if (!EsSiatOnline)
        {
            _logger.LogWarning("Los servicios de Impuestos Nacionales no estan activos");
            return;
        }
        // Libelula
        string? Appkey = await _context.FacConfiguraciones.AsNoTracking().Where(m => m.Estado).Select(m => m.AppKey).FirstOrDefaultAsync();
        bool EsLibelulaOnline = await _libelulaApi.EsOnline(Appkey);
        if (!EsLibelulaOnline)
        {
            _logger.LogWarning("Los servicios de Libelula/Todotix no estan activos");
            return;
        }
        // Consultar datos
        _logger.LogInformation("Consulta de datos iniciado {DateTime.Now.ToLongTimeString()}", DateTime.Now.ToLongTimeString());
        List<FacturaElecConsulta> FacturasConsulta = await _context.FacturaElecConsultas.AsNoTracking().ToListAsync();
        ConcurrentBag<ConsultaDatosReq> FacturasPeticion = new();
        Parallel.ForEach(FacturasConsulta, options, (item) =>
        {
            FacturasPeticion.Add(new ConsultaDatosReq() { AppKey = item.AppKey ?? "", IdFactura = item.IdFactura });
        });
        // Proceso
        foreach (ConsultaDatosReq request in FacturasPeticion)
        {
            // Metodo POST
            ConsultaDatosRes? response = await _libelulaApi.ConsultaFacturas(request);
            contador++;
            // Guardado de logs de peticiones
            FacRegistroJson jsonLogs = new()
            {
                TipoJsonId = 3,
                JsonRequest = JsonSerializer.Serialize(request),
                JsonResponse = JsonSerializer.Serialize(response)
            };
            _ = await _context.FacRegistroJsons.AddAsync(jsonLogs);
            // Respuesta Nula
            if (response is null)
            {
                FacRegistroEvento eventoNull = new()
                {
                    IdFactura = request.IdFactura,
                    Error = 1,
                    Existente = 0,
                    Mensaje = "Respuesta nula de Libelula en consulta de datos",
                    IdTransaccion = "",
                    UrlPasarelaPagos = ""
                };
                _ = await _context.FacRegistroEventos.AddAsync(eventoNull);
                _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {request.IdFactura}, 16");
                continue;
            }
            // Registro de la respuesta de Libelula
            FacRegistroEvento evento = new()
            {
                IdFactura = request.IdFactura,
                Error = response.Error,
                Existente = response.Existente,
                Mensaje = response.Mensaje,
                IdTransaccion = "",
                UrlPasarelaPagos = ""
            };
            _ = await _context.FacRegistroEventos.AddAsync(evento);
            // Evaluacion de Respuesta
            switch (response.Error)
            {
                case 0:
                    if (response.Datos is null)
                    {
                        // No se encontraron pagos
                        _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {request.IdFactura}, 22");
                        continue;
                    }
                    ConsultaFactura? factura = response.Datos.Facturas?.FirstOrDefault();
                    if (factura is null)
                    {
                        // Existe pago pero no facturas
                        _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {request.IdFactura}, 22");
                        continue;
                    }
                    AoVentasCobranzaFac? facturaMaestro = await _context.AoVentasCobranzaFacs.FindAsync(request.IdFactura);
                    if (facturaMaestro is null)
                    {
                        // No se encontraron registros
                        continue;
                    }
                    // Factura
                    facturaMaestro.NroFactura = factura.NumeroFactura;
                    facturaMaestro.DosificaAutorizacion = factura.NumeroAutorizacion;
                    facturaMaestro.UrlTodotix = factura.UrlLibelula;
                    facturaMaestro.UrlSinSfe = factura.NumeroAutorizacion.Length > 1 ? Utilitario.CreatorUrlSinSfe(facturaMaestro.CodigoEmpresa ?? 1, factura.NumeroAutorizacion, factura.NumeroFactura) : null;
                    facturaMaestro.EstadoFacId = factura.NumeroAutorizacion.Length > 1 ? 8 : 7;
                    _context.Entry(facturaMaestro).State = EntityState.Modified;
                    break;
                default:
                    // Error en consulta
                    _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {request.IdFactura}, 22");
                    break;
            }
            // Guardado de cambios por lote
            if (contador % numeroLote == 0)
            {
                try
                {
                    _ = await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException dbError)
                {
                    _logger.LogError("Error facturacion: {db.Message} {DateTime.Now.ToLongTimeString()}", dbError.Message, DateTime.Now.ToLongTimeString());
                    //throw;
                }
            }
        }
        // Guardado de cambios fuera del lote
        try
        {
            _ = await _context.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException dbError)
        {
            _logger.LogError("Error facturacion: {db.Message} {DateTime.Now.ToLongTimeString()}", dbError.Message, DateTime.Now.ToLongTimeString());
            //throw;
        }
        _logger.LogInformation("Consulta de datos finalizado {DateTime.Now.ToLongTimeString()}", DateTime.Now.ToLongTimeString());
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("HostedService: Consulta de datos de facturas deteniendose {DateTime.Now.ToLongTimeString()}", DateTime.Now.ToLongTimeString());
        _timer?.Change(Timeout.Infinite, 0);
        return Task.CompletedTask;
    }

    public void Dispose()
    {
        _timer?.Dispose();
        GC.SuppressFinalize(this);
    }
}
