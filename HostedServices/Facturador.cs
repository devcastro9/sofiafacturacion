﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MimeKit;
using SofiaFacturacion.Data;
using SofiaFacturacion.Models;
using SofiaFacturacion.Services;
using SofiaFacturacion.Utility;
using SofiaFacturacion.ViewModels;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace SofiaFacturacion.HostedServices;

public class Facturador : IHostedService, IDisposable
{
    private readonly IServiceScopeFactory _scopeFactory;
    private readonly LibelulaApi _libelulaApi;
    private readonly AbstractApi _abstractApi;
    private readonly ITestInternet _internet;
    public ILogger<Facturador> _logger;
    private Timer? _timer = null;
    public Facturador(IServiceScopeFactory scopeFactory, LibelulaApi libelulaApi, AbstractApi abstractApi, ITestInternet internet, ILogger<Facturador> logger)
    {
        _scopeFactory = scopeFactory;
        _libelulaApi = libelulaApi;
        _abstractApi = abstractApi;
        _internet = internet;
        _logger = logger;
    }
    public Task StartAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("Hosted service facturador iniciando {DateTime.Now.ToLongTimeString()}", DateTime.Now.ToLongTimeString());
        _timer = new Timer(Facturar, null, TimeSpan.FromSeconds(10), TimeSpan.FromMinutes(2));
        return Task.CompletedTask;
    }

    private async void Facturar(object? state)
    {
        int contador = 0;
        int numeroLote = 5;
        ParallelOptions options = new() { MaxDegreeOfParallelism = Environment.ProcessorCount };
        using IServiceScope ambito = _scopeFactory.CreateScope();
        AppDbContext _context = ambito.ServiceProvider.GetRequiredService<AppDbContext>();
        ISiatFacturacion _siat = ambito.ServiceProvider.GetRequiredService<ISiatFacturacion>();
        IEmailSmtp _email = ambito.ServiceProvider.GetRequiredService<IEmailSmtp>();
        // Internet
        bool EsInternetOnline = await _internet.ExisteInternet();
        if (!EsInternetOnline)
        {
            _logger.LogWarning("No existe una conexion activa a internet");
            return;
        }
        // SIAT
        string? TokenSiat = await _context.FacConfiguraciones.AsNoTracking().Where(m => m.Estado).Select(m => m.TokenDelegado).FirstOrDefaultAsync();
        bool EsSiatOnline = await _siat.VerificarComunicacionSiat(TokenSiat);
        if (!EsSiatOnline)
        {
            _logger.LogWarning("Los servicios de Impuestos Nacionales no estan activos");
            return;
        }
        // Libelula
        string? Appkey = await _context.FacConfiguraciones.AsNoTracking().Where(m => m.Estado).Select(m => m.AppKey).FirstOrDefaultAsync();
        bool EsLibelulaOnline = await _libelulaApi.EsOnline(Appkey);
        if (!EsLibelulaOnline)
        {
            _logger.LogWarning("Los servicios de Libelula/Todotix no estan activos");
            return;
        }
        // Consultar datos
        _logger.LogInformation("Facturacion iniciada {DateTime.Now.ToLongTimeString()}", DateTime.Now.ToLongTimeString());
        List<FacturaElec> FacMaestro = await _context.FacturaElecs.AsNoTracking().ToListAsync();
        foreach (FacturaElec item in FacMaestro)
        {
            // Detalles
            List<FacturaElecDetalle> FacDetalle = await _context.FacturaElecDetalles.AsNoTracking().Where(x => x.IdFactura == item.IdFactura).ToListAsync();
            // Email de alerta por defecto
            MimeMessage emailDefecto = _email.EmisionFacturaEmail("Hubo un problema al facturar.", item.IdFactura, item.Total ?? 0, item.Nit ?? "0", item.RazonSocial ?? "N/A");
            // Detalles Validacion
            if (FacDetalle.Count == 0)
            {
                FacRegistroEvento eventoNoDetalle = new()
                {
                    IdFactura = item.IdFactura,
                    Error = 1,
                    Existente = 0,
                    Mensaje = "No se encontraron registros de detalles",
                    IdTransaccion = "",
                    UrlPasarelaPagos = ""
                };
                _ = await _context.FacRegistroEventos.AddAsync(eventoNoDetalle);
                _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {item.IdFactura}, 17");
                _ = await _email.EnviarEmail(emailDefecto);
                continue;
            }
            // Metadatos
            List<FacturaElecMetadato> FacMetadatos = await _context.FacturaElecMetadatos.AsNoTracking().Where(m => m.IdFactura == item.IdFactura).ToListAsync();
            if (FacMetadatos.Count == 0)
            {
                FacRegistroEvento eventoNoMetadato = new()
                {
                    IdFactura = item.IdFactura,
                    Error = 1,
                    Existente = 0,
                    Mensaje = "No se encontraron registros de metadatos",
                    IdTransaccion = "",
                    UrlPasarelaPagos = ""
                };
                _ = await _context.FacRegistroEventos.AddAsync(eventoNoMetadato);
                _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {item.IdFactura}, 17");
                _ = await _email.EnviarEmail(emailDefecto);
                continue;
            }
            // Validacion de NIT
            if (item.ValidarNit && item.TipoDoc == "NIT")
            {
                if (!long.TryParse(item.Nit, out long nit_convertido))
                {
                    FacRegistroEvento eventoNoMetadato = new()
                    {
                        IdFactura = item.IdFactura,
                        Error = 1,
                        Existente = 0,
                        Mensaje = "El NIT no es valido",
                        IdTransaccion = "",
                        UrlPasarelaPagos = ""
                    };
                    _ = await _context.FacRegistroEventos.AddAsync(eventoNoMetadato);
                    _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {item.IdFactura}, 17");
                    _ = await _email.EnviarEmail(emailDefecto);
                    continue;
                }
            }
            // Validacion de Email
            if (item.ValidarEmail)
            {
                FacRegistroEvento eventoEmailNoValido = new()
                {
                    IdFactura = item.IdFactura,
                    Error = 1,
                    Existente = 0,
                    Mensaje = "El email no es valido",
                    IdTransaccion = "",
                    UrlPasarelaPagos = ""
                };
                MimeMessage emailProblema = _email.EmisionFacturaEmail("El email enviado no es valido.", item.IdFactura, item.Total ?? 0, item.Nit ?? "0", item.RazonSocial ?? "N/A");
                if (item.Email is null || !Utilitario.IsValidEmail(item.Email))
                {
                    _ = await _context.FacRegistroEventos.AddAsync(eventoEmailNoValido);
                    _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {item.IdFactura}, 11");
                    _ = await _email.EnviarEmail(emailProblema);
                    continue;
                }
                // REST Api Abstract Api
                string ApiKey = await (from c in _context.FacKeys where c.IdKey == 1 select c.Appkey).AsNoTracking().FirstAsync();
                AbstractRes? responseEmail = await _abstractApi.ValidarEmail(ApiKey, item.Email);
                if (responseEmail is null)
                {
                    _ = await _context.FacRegistroEventos.AddAsync(eventoEmailNoValido);
                    _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {item.IdFactura}, 11");
                    _ = await _email.EnviarEmail(emailProblema);
                    continue;
                }
                if ((responseEmail.EsSmtpValido.Valor is null || responseEmail.EsSmtpValido.Valor == false || responseEmail.Entregable != "DELIVERABLE") && responseEmail.Puntuacion < 0.7m)
                {
                    // Dominio en listas negras
                    FacDominioEmail? EmailDominio = await _context.FacDominioEmails.AsNoTracking().Where(m => m.DominioIgnorado == Utilitario.ExtractDomain(responseEmail.Email) && m.Habilitado == true).FirstOrDefaultAsync();
                    if (EmailDominio is null)
                    {
                        _ = await _context.FacRegistroEventos.AddAsync(eventoEmailNoValido);
                        _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {item.IdFactura}, 11");
                        _ = await _email.EnviarEmail(emailProblema);
                        continue;
                    }
                }
            }
            // Montos
            decimal total_detalle = FacDetalle.Sum(b => b.Costo * b.Cantidad) ?? 0;
            total_detalle = Math.Round(total_detalle, 2);
            FacRegistroEvento eventoMonto = new()
            {
                IdFactura = item.IdFactura,
                Error = 1,
                Existente = 0,
                Mensaje = "Problema con los montos en cabecera o detalle",
                IdTransaccion = "",
                UrlPasarelaPagos = ""
            };
            if (item.Total is null || total_detalle == 0)
            {
                _ = await _context.FacRegistroEventos.AddAsync(eventoMonto);
                _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {item.IdFactura}, 15");
                _ = await _email.EnviarEmail(emailDefecto);
                continue;
            }
            if (item.Total != total_detalle)
            {
                _ = await _context.FacRegistroEventos.AddAsync(eventoMonto);
                _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {item.IdFactura}, 15");
                _ = await _email.EnviarEmail(emailDefecto);
                continue;
            }
            // Codigo para SIN no valido
            bool detalleNoHomologado = FacDetalle.Exists(x => x.Producto is null || x.Producto.Length < 3);
            FacRegistroEvento eventoNoHomologado = new()
            {
                IdFactura = item.IdFactura,
                Error = 1,
                Existente = 0,
                Mensaje = "Problema con la homologacion",
                IdTransaccion = "",
                UrlPasarelaPagos = ""
            };
            MimeMessage emailHomologado = _email.EmisionFacturaEmail("Hubo un problema con la homologacion.", item.IdFactura, item.Total ?? 0, item.Nit ?? "0", item.RazonSocial ?? "N/A");
            if (detalleNoHomologado)
            {
                _ = await _context.FacRegistroEventos.AddAsync(eventoNoHomologado);
                _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {item.IdFactura}, 14");
                _ = await _email.EnviarEmail(emailHomologado);
                continue;
            }
            // Request (Peticion)
            RegistroPagoReq request = new()
            {
                AppKey = item.AppKey ?? "",
                IdFactura = item.IdFactura,
                EmailCliente = item.Email ?? "otisbolivia@gmail.com",
                CallbackUrl = item.Curl + item.IdFactura,
                Descripcion = item.FacturaNota ?? "SFE",
                RazonSocial = item.RazonSocial ?? "S/N",
                NumeroDocumento = item.Nit ?? "0",
                CodigoTipoDocumento = item.TipoDoc ?? "NIT",
                CodigoCliente = item.CodigoCliente.ToString(),
                FacturaNotaCliente = item.FacturaNota ?? "SFE",
                TipoFactura = item.TipoFactura,
                EmiteFactura = item.EmiteFactura != 0,
                Moneda = item.TipoMoneda,
                TipoCambio = item.CambioOficial,
                CanalCaja = item.CanalCaja,
                CanalCajaSucursal = item.CanalCajaSucursal,
                CanalCajaUsuario = "Facturacion " + item.CanalCajaUsuario,
                MetodoPago = item.MetodoPago ?? "EFECTIVO"
            };
            // Adicion de detalles
            ConcurrentBag<RegistroPagoDetalle> reqDetalle = new();
            Parallel.ForEach(FacDetalle, options, (d) =>
            {
                reqDetalle.Add(new RegistroPagoDetalle()
                {
                    ActividadEconomica = d.Actividad,
                    CodigoProductoSin = d.ProductoSin ?? 0,
                    CodigoProducto = d.Producto ?? "",
                    Concepto = d.Concepto ?? "",
                    Cantidad = d.Cantidad ?? 0,
                    UnidadMedida = d.UnidadMedida ?? 0,
                    CostoUnitario = d.Costo ?? 0,
                    Cobranza = d.Cobranza ?? 0
                });
            });
            request.FacturaDetalles = reqDetalle.OrderBy(m => m.Cobranza).ToHashSet();
            // Adicion de metadatos
            ConcurrentBag<RegistroPagoMetadato> reqMetadata = new();
            Parallel.ForEach(FacMetadatos, options, (m) =>
            {
                reqMetadata.Add(new RegistroPagoMetadato()
                {
                    Dato = m.Dato ?? "",
                    Nombre = m.Nombre
                });
            });
            request.FacturaMetadatos = reqMetadata.ToHashSet();
            // Metodo POST
            RegistroPagoRes? response = await _libelulaApi.RegistrarFactura(request);
            contador++;
            // Guardado de logs de peticiones
            FacRegistroJson jsonLogs = new()
            {
                TipoJsonId = 1,
                JsonRequest = JsonSerializer.Serialize(request),
                JsonResponse = JsonSerializer.Serialize(response)
            };
            _ = await _context.FacRegistroJsons.AddAsync(jsonLogs);
            // Respuesta Nula
            if (response is null || response.Mensaje is null)
            {
                FacRegistroEvento eventoNull = new()
                {
                    IdFactura = item.IdFactura,
                    Error = 1,
                    Existente = 0,
                    Mensaje = "Respuesta nula de Libelula",
                    IdTransaccion = "",
                    UrlPasarelaPagos = ""
                };
                _ = await _context.FacRegistroEventos.AddAsync(eventoNull);
                _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {item.IdFactura}, 14");
                _ = await _email.EnviarEmail(emailDefecto);
                continue;
            }
            // Registro de la respuesta de Libelula
            FacRegistroEvento evento = new()
            {
                IdFactura = item.IdFactura,
                Error = response.Error,
                Existente = response.Existente,
                Mensaje = response.Mensaje,
                IdTransaccion = response.IdTransaccion,
                UrlPasarelaPagos = response.UrlPasarelaPagos
            };
            _ = await _context.FacRegistroEventos.AddAsync(evento);
            // Error Interno de Libelula
            if (response.Mensaje.Contains("INTERNO"))
            {
                _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {item.IdFactura}, 13");
                _ = await _email.EnviarEmail(emailDefecto);
                continue;
            }
            // Estado HTTP no valido
            if (response.Mensaje.Contains("ERROR"))
            {
                _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {item.IdFactura}, 14");
                _ = await _email.EnviarEmail(emailDefecto);
                continue;
            }
            // Registro no encontrado
            AoVentasCobranzaFac? cobranza_fac = await _context.AoVentasCobranzaFacs.FindAsync(item.IdFactura);
            if (cobranza_fac is null)
            {
                _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {item.IdFactura}, 17");
                _ = await _email.EnviarEmail(emailDefecto);
                continue;
            }
            // Factura electronica
            FacturaElectronicaRes? facturaEmitida = (from f in response.FacturasElectronicas select f).FirstOrDefault();
            if (facturaEmitida is null)
            {
                _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {item.IdFactura}, 14");
                _ = await _email.EnviarEmail(emailDefecto);
                continue;
            }
            switch ((response.Error, response.Existente))
            {
                case (2, 1):
                case (0, 0):
                    cobranza_fac.Identificador = facturaEmitida.Identificador;
                    cobranza_fac.NroFactura = facturaEmitida.NumeroFactura;
                    cobranza_fac.DosificaAutorizacion = facturaEmitida.NumeroAutorizacion;
                    cobranza_fac.FechaFac = facturaEmitida.FechaPagoCanal;
                    cobranza_fac.FechaFacSin = facturaEmitida.FechaPagoCanal;
                    cobranza_fac.Cufd = facturaEmitida.Cufd;
                    cobranza_fac.UrlSinSfe = facturaEmitida.UrlSinSfe;
                    cobranza_fac.UrlTodotix = facturaEmitida.Url;
                    cobranza_fac.EstadoFacId = facturaEmitida.NumeroAutorizacion?.Length > 1 ? 8 : 12;
                    _context.Entry(cobranza_fac).State = EntityState.Modified;
                    if (response.Error == 0)
                    {
                        _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[conta_fac_elec] {item.IdFactura}");
                        _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facRespaldoDetalle] {item.IdFactura}");
                    }
                    break;
                default:
                    _ = await _context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[facEstadoFacturacion] {item.IdFactura}, 14");
                    break;
            }
            if (cobranza_fac.EstadoFacId == 12)
            {
                MimeMessage emailDosificacion = _email.EmisionFacturaEmail("Hay un problema con la dosificacion de la factura.", item.IdFactura, item.Total ?? 0, item.Nit ?? "0", item.RazonSocial ?? "N/A");
                _ = await _email.EnviarEmail(emailDosificacion);
            }
            if (cobranza_fac.EstadoFacId == 14)
            {
                _ = await _email.EnviarEmail(emailDefecto);
            }
            // Guardado de cambios por lote
            if (contador % numeroLote == 0)
            {
                try
                {
                    _ = await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException dbError)
                {
                    _logger.LogError("Error facturacion: {db.Message} {DateTime.Now.ToLongTimeString()}", dbError.Message, DateTime.Now.ToLongTimeString());
                    //throw;
                }
            }
        }
        // Guardado de cambios fuera del lote
        try
        {
            _ = await _context.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException dbError)
        {
            _logger.LogError("Error facturacion: {db.Message} {DateTime.Now.ToLongTimeString()}", dbError.Message, DateTime.Now.ToLongTimeString());
            //throw;
        }
        _logger.LogInformation("Facturacion finalizada {DateTime.Now.ToLongTimeString()}", DateTime.Now.ToLongTimeString());
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("Hosted Service facturador deteniendose.");
        _timer?.Change(Timeout.Infinite, 0);
        return Task.CompletedTask;
    }
    public void Dispose()
    {
        _timer?.Dispose();
        GC.SuppressFinalize(this);
    }
}
