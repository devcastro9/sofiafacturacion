﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace SofiaFacturacion.Utility;

public class FormatDateTimeConverter : JsonConverter<DateTime>
{
    private readonly string Format;
    /// <summary>
    /// Conversor de DateTime personalizado con formato (no acepta valores null).
    /// El conversor de System.Text.Json solo admite el formato ISO 8601-1:2019
    /// </summary>
    /// <param name="format">Especificador del formato en cadena para Datetime</param>
    public FormatDateTimeConverter(string format)
    {
        Format = format;
    }
    public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        return DateTime.ParseExact(reader.GetString() ?? "", Format, null);
    }
    public override void Write(Utf8JsonWriter writer, DateTime value, JsonSerializerOptions options)
    {
        writer.WriteStringValue(value.ToString(Format));
    }
}
