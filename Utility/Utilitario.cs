﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;

namespace SofiaFacturacion.Utility;

public class Utilitario
{
    /// <summary>
    /// Funcion para determinar si un email es sintact
    /// </summary>
    /// <param name="email">Email a validar</param>
    /// <returns>Un valor booleano que representa si es valido o no</returns>
    public static bool IsValidEmail(string email)
    {
        if (string.IsNullOrWhiteSpace(email))
        {
            return false;
        }
        try
        {
            // Normalizacion del dominio
            email = Regex.Replace(email, @"(@)(.+)$", DomainMapper, RegexOptions.None, TimeSpan.FromMilliseconds(200));

            // Examina la parte del dominio del correo electrónico y la normaliza.
            static string DomainMapper(Match match)
            {
                // Utilice la clase IdMapping para convertir nombres de dominio Unicode.
                IdnMapping idn = new();

                // Extraiga y procese el nombre de dominio (arroja ArgumentException en no válido)
                string domainName = idn.GetAscii(match.Groups[2].Value);

                return match.Groups[1].Value + domainName;
            }
        }
        catch (RegexMatchTimeoutException)
        {
            return false;
        }
        catch (ArgumentException)
        {
            return false;
        }
        try
        {
            return Regex.IsMatch(email, @"^[^@\s]+@[^@\s]+\.[^@\s]+$", RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
        }
        catch (RegexMatchTimeoutException)
        {
            return false;
        }
    }

    /// <summary>
    /// Funcion para extraer el dominio de un email
    /// </summary>
    /// <param name="email">Parametro de email</param>
    /// <returns>El dominio del email</returns>
    public static string ExtractDomain(string email)
    {
        Match result = Regex.Match(email, @"(@)(.+)$", RegexOptions.IgnoreCase);
        if (result.Success)
        {
            return result.Groups[2].Value.ToLower();
        }
        else
        {
            return "";
        }
    }

    public static string CreatorUrlSinSfe(int EmpresaId, string Dosificacion, int numeroFac)
    {
        Dictionary<int, int> empresas = new()
        {
            { 1, 1018533029 },
            { 2, 125887020 }
        };
        return $"https://siat.impuestos.gob.bo/consulta/QR?nit={empresas[EmpresaId]}&cuf={Dosificacion}&numero={numeroFac}&t=1";
    }
}
