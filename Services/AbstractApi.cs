﻿using SofiaFacturacion.ViewModels;
using System;
using System.Net.Http;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace SofiaFacturacion.Services;

/// <summary>
/// Http Client tipado de AbstractApi para la validacion de email
/// </summary>
public class AbstractApi
{
    private readonly HttpClient _httpClient;
    private readonly int _retardo = 120; // Plan de 9USD
    public AbstractApi(HttpClient httpClient)
    {
        _httpClient = httpClient;
        _httpClient.BaseAddress = new Uri(@"https://emailvalidation.abstractapi.com/v1/");
    }

    /// <summary>
    /// Validador de Email
    /// </summary>
    /// <param name="ApiKey">Apikey de AbstractApi</param>
    /// <param name="email">Email a validar en formato string</param>
    /// <returns>Retorna la respuesta de AbstractApi</returns>
    public async Task<AbstractRes?> ValidarEmail(string ApiKey, string email)
    {
        try
        {
            // Enviamos peticion GET
            using HttpResponseMessage response = await _httpClient.GetAsync($"?api_key={ApiKey}&email={email}");
            // Retardo de Plan
            await Task.Delay(_retardo);
            // Codigo HTTP incorrecto enviado a excepcion
            response.EnsureSuccessStatusCode();
            JsonSerializerOptions options = new()
            {
                NumberHandling = JsonNumberHandling.AllowReadingFromString,
                ReferenceHandler = ReferenceHandler.IgnoreCycles
            };
            string result = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<AbstractRes>(result, options);
        }
        catch (Exception)
        {
            return null;
        }
    }

    /// <summary>
    /// Validador de Email
    /// </summary>
    /// <param name="ApiKey">Apikey de AbstractApi</param>
    /// <param name="email">Email a validar en formato string</param>
    /// <param name="auto_correct">Define si existe autocorreccion</param>
    /// <returns>Retorna la respuesta de AbstractApi</returns>
    public async Task<AbstractRes?> ValidarEmail(string ApiKey, string email, bool auto_correct = true)
    {
        try
        {
            // Enviamos peticion GET
            using HttpResponseMessage response = await _httpClient.GetAsync($"?api_key={ApiKey}&email={email}&auto_correct={auto_correct}");
            // Retardo de Plan
            await Task.Delay(_retardo);
            // Codigo HTTP incorrecto enviado a excepcion
            response.EnsureSuccessStatusCode();
            JsonSerializerOptions options = new()
            {
                NumberHandling = JsonNumberHandling.AllowReadingFromString,
                ReferenceHandler = ReferenceHandler.IgnoreCycles
            };
            string result = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<AbstractRes>(result, options);
        }
        catch (Exception)
        {
            return null;
        }
    }
}
