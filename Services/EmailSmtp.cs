﻿using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using System;
using System.Threading.Tasks;

namespace SofiaFacturacion.Services;

public class EmailSmtp : IEmailSmtp, IDisposable
{
    private readonly SmtpClient _smtpClient;
    private string SmtpServer { get; set; }
    private int SmtpPort { get; set; }
    public string SenderEmail { get; set; }
    private string Passwd { get; set; }
    private string Email { get; set; }
    private string ConCopia { get; set; }

    public EmailSmtp(string Server, int Port, string EmailSender, string Password, string EmailDestino, string ConCopy)
    {
        SmtpServer = Server;
        SmtpPort = Port; // Error 587
        SenderEmail = EmailSender;
        Passwd = Password;
        Email = EmailDestino;
        ConCopia = ConCopy;
        _smtpClient = new SmtpClient
        {
            Timeout = 10000
        };
    }

    public async Task<bool> EnviarEmail(MimeMessage message)
    {
        try
        {
            await _smtpClient.ConnectAsync(SmtpServer, SmtpPort, SecureSocketOptions.SslOnConnect);
            await _smtpClient.AuthenticateAsync(SenderEmail, Passwd);
            await _smtpClient.SendAsync(message);
            await _smtpClient.DisconnectAsync(true);
            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }

    public MimeMessage EmisionFacturaEmail(string Mensaje, long Id, decimal Monto, string CodigoCliente, string Cliente)
    {
        MimeMessage message = new();
        message.From.Add(new MailboxAddress("Notificación automática", SenderEmail));
        message.To.Add(new MailboxAddress("Facturación", Email));
        message.Cc.Add(new MailboxAddress("Sistemas", ConCopia));
        message.Subject = "Problema de factura al emitir";
        message.Body = new TextPart("html")
        {
            Text = @$"<div style=""font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;"">
                            <h2>Alerta de la factura #{Id}</h2>
                            <p>{Mensaje}</p>
                            <ul>
                                <li>Monto: {Monto} Bs</li>
                                <li>Cliente: {CodigoCliente} - {Cliente}</li>
                            </ul><br>
                            <span style=""opacity: 0.7;font-size: small;"">Esta es una notificaci&oacute;n autom&aacute;tica de sistema</span>
                       </div>"
        };
        return message;
    }

    public MimeMessage AnulacionFacturaEmail(string Mensaje, long Id, decimal Monto, string CodigoCliente, string Cliente, string UrlFac)
    {
        MimeMessage message = new();
        message.From.Add(new MailboxAddress("Notificación automática", SenderEmail));
        message.To.Add(new MailboxAddress("Facturación", Email));
        message.Cc.Add(new MailboxAddress("Sistemas", ConCopia));
        message.Subject = "Problema de factura al anular";
        message.Body = new TextPart("html")
        {
            Text = @$"<div style=""font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;"">
                            <h2>Alerta de la Factura #{Id}</h2>
                            <p>{Mensaje}</p>
                            <ul>
                                <li>Monto: {Monto} Bs</li>
                                <li>Cliente: {CodigoCliente} - {Cliente}</li>
                            </ul>
                            <a href=""{UrlFac}"">Link de la factura</a><br>
                            <span style=""opacity: 0.7;font-size: small;"">Esta es una notificaci&oacute;n autom&aacute;tica de sistema</span>
                       </div>"
        };
        return message;
    }

    public void Dispose()
    {
        _smtpClient.Dispose();
        GC.SuppressFinalize(this);
    }
}
