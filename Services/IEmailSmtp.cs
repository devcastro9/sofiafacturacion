﻿using MimeKit;
using System.Threading.Tasks;

namespace SofiaFacturacion.Services;

public interface IEmailSmtp
{
    public Task<bool> EnviarEmail(MimeMessage message);

    public MimeMessage EmisionFacturaEmail(string Mensaje, long Id, decimal Monto, string CodigoCliente, string Cliente);

    public MimeMessage AnulacionFacturaEmail(string Mensaje, long Id, decimal Monto, string CodigoCliente, string Cliente, string UrlFac);
}
