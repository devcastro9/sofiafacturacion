﻿using SofiaFacturacion.Utility;
using SofiaFacturacion.ViewModels;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace SofiaFacturacion.Services;
/// <summary>
/// Servicio para la REST Api de Libelula/Todotix
/// </summary>
public class LibelulaApi
{
    private readonly HttpClient _httpClient;
    private readonly int _retardo = 100;
    public LibelulaApi(HttpClient httpClient)
    {
        _httpClient = httpClient;
        _httpClient.BaseAddress = new Uri(@"https://api.todotix.com/rest/");
    }

    /// <summary>
    /// Metodo de registro de pago en caja que tambien genera factura electronica
    /// </summary>
    /// <param name="pago">Objeto Factura con los datos a enviar para emitir la factura</param>
    /// <returns>Respuesta de la peticion en formato objeto</returns>
    public async Task<RegistroPagoRes?> RegistrarFactura(RegistroPagoReq pago)
    {
        try
        {
            // Body del json para la peticion POST
            string JsonBody = JsonSerializer.Serialize(pago);
            // Serializacion del objeto Factura
            StringContent content = new(JsonBody, Encoding.UTF8, Application.Json);
            // Consumo del metodo POST
            using HttpResponseMessage result = await _httpClient.PostAsync("deuda/registrar", content);
            // Retardo para evitar sobrecarga a Libelula
            await Task.Delay(_retardo);
            // Generamos una excepcion si el codigo HTTP indica un error
            result.EnsureSuccessStatusCode();
            // Deserializacion de la respuesta
            JsonSerializerOptions options = new()
            {
                ReferenceHandler = ReferenceHandler.IgnoreCycles
            };
            options.Converters.Add(new FormatDateTimeNullConverter("yyyy-MM-dd HH:mm:ss"));
            string ContentResponse = await result.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<RegistroPagoRes>(ContentResponse, options);
        }
        catch (Exception ex)
        {
            return new RegistroPagoRes() { Error = 2, Existente = 2, Mensaje = ex.Message };
        }
    }

    /// <summary>
    /// Metodo de anulacion de pago en caja que tambien anula la factura
    /// </summary>
    /// <param name="anula">Objeto que representa los datos a enviar para anular la factura</param>
    /// <returns>Respuesta de la peticion en formato de objeto</returns>
    public async Task<AnulaPagoRes?> AnularFactura(AnulaPagoReq anula)
    {
        try
        {
            // Serializacion del objeto Anula
            string JsonBody = JsonSerializer.Serialize(anula);
            StringContent content = new(JsonBody, Encoding.UTF8, Application.Json);
            // Consumo del metodo POST
            using HttpResponseMessage result = await _httpClient.PostAsync("deuda/anular_pagos", content);
            // Retardo para evitar sobrecarga a Libelula
            await Task.Delay(_retardo);
            // Caso duplicado
            if (result.StatusCode == HttpStatusCode.Forbidden)
            {
                AnulaPagoRes? duplicado = JsonSerializer.Deserialize<AnulaPagoRes>(await result.Content.ReadAsStringAsync());
                if (duplicado != null)
                {
                    return new AnulaPagoRes() { Estado = false, Mensaje = duplicado.Mensaje };
                }
            }
            // Codigo HTTP incorrecto enviado a excepcion
            result.EnsureSuccessStatusCode();
            // Codigo de estado HTTP Correcto
            string ContentResponse = await result.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<AnulaPagoRes>(ContentResponse);
        }
        catch (Exception ex)
        {
            return new AnulaPagoRes() { Estado = false, Mensaje = ex.Message };
        }
    }

    /// <summary>
    /// Metodo de consulta de pagos en caja
    /// </summary>
    /// <param name="datos">Objeto que representa los datos a enviar para anular la factura</param>
    /// <returns>Respuesta de la peticion en formato de objeto</returns>
    public async Task<ConsultaDatosRes?> ConsultaFacturas(ConsultaDatosReq datos)
    {
        try
        {
            // Serializacion del objeto Anula
            string JsonBody = JsonSerializer.Serialize(datos);
            StringContent content = new(JsonBody, Encoding.UTF8, Application.Json);
            // Consumo del metodo POST
            using HttpResponseMessage result = await _httpClient.PostAsync("deuda/consultar_deudas/por_identificador", content);
            // Retardo para evitar sobrecarga a Libelula
            await Task.Delay(_retardo);
            // Codigo HTTP incorrecto enviado a excepcion
            result.EnsureSuccessStatusCode();
            // Codigo de estado HTTP Correcto
            string ContentResponse = await result.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<ConsultaDatosRes>(ContentResponse);
        }
        catch (Exception ex)
        {
            return new ConsultaDatosRes() { Error = 1, Existente = 0, Mensaje = ex.Message };
        }
    }

    /// <summary>
    /// Verifica si Libelula esta online
    /// </summary>
    /// <param name="appkey">Appkey asignado por libelula</param>
    /// <returns>El estado booleano de Libelula y el appkey</returns>
    public async Task<bool> EsOnline(string? appkey)
    {
        try
        {
            if (appkey == null)
            {
                return false;
            }
            using HttpResponseMessage result = await _httpClient.GetAsync($"clientes/lista?appkey={appkey}");
            result.EnsureSuccessStatusCode();
            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }
}
