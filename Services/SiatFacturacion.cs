﻿using SiatSoapFacturacion;
using SofiaFacturacion.Utility;
using System;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Xml;

namespace SofiaFacturacion.Services;

public class SiatFacturacion : ISiatFacturacion
{
    private static readonly string endpointAddress = "https://pilotosiatservicios.impuestos.gob.bo/v2/ServicioFacturacionElectronica?wsdl";
    /// <summary>
    /// Metodo de verificacion de comunicacion con Impuestos Nacionales
    /// </summary>
    /// <returns>
    /// Retorna un valor booleano
    /// </returns>
    public async Task<bool> VerificarComunicacionSiat(string? token_siat)
    {
        if (token_siat == null)
        {
            return false;
        }
        bool EstadoConexion = false;
        BasicHttpBinding binding = new()
        {
            SendTimeout = TimeSpan.FromSeconds(1000),
            MaxBufferSize = int.MaxValue,
            MaxReceivedMessageSize = int.MaxValue,
            AllowCookies = true,
            ReaderQuotas = XmlDictionaryReaderQuotas.Max,
        };
        // Modo de conexion HTTP/HTTPS
        //binding.Security.Mode = BasicHttpSecurityMode.None; // http
        binding.Security.Mode = BasicHttpSecurityMode.Transport; // https
        ServicioFacturacionClient servicio = new(binding, new EndpointAddress(endpointAddress));
        servicio.Endpoint.EndpointBehaviors.Add(new CustomAuthenticationBehaviour(token_siat));
        try
        {
            await servicio.OpenAsync();
            verificarComunicacionResponse response = await servicio.verificarComunicacionAsync();
            EstadoConexion = response.@return.transaccion;
        }
        catch (Exception)
        {
            EstadoConexion = false;
        }
        finally
        {
            servicio.Close();
        }
        return EstadoConexion;
    }
}
