﻿using System.Threading.Tasks;

namespace SofiaFacturacion.Services;

public interface ISiatFacturacion
{
    public Task<bool> VerificarComunicacionSiat(string? token_siat);
}
