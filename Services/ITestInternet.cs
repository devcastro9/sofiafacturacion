﻿using System.Threading.Tasks;

namespace SofiaFacturacion.Services
{
    public interface ITestInternet
    {
        public Task<bool> ExisteInternet();
    }
}
