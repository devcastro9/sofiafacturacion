﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace SofiaFacturacion.Services;

/// <summary>
/// Servicio de Validacion de email
/// </summary>
public class ViewSiatFacEstado
{
    private readonly HttpClient _httpClient;
    public ViewSiatFacEstado(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }

    /// <summary>
    /// Funcion para verificar si una factura esta anulada en el Servicio de Impuestos
    /// </summary>
    /// <param name="UrlFac">Url de la factura</param>
    /// <returns>Estado de la factura</returns>
    public async Task<bool?> EsFacturaAnulada(string UrlFac)
    {
        try
        {
            using HttpResponseMessage response = await _httpClient.GetAsync(UrlFac);
            response.EnsureSuccessStatusCode();
            string result = await response.Content.ReadAsStringAsync();
            return result.Contains("<label>ANULADA</label>");
        }
        catch (Exception)
        {
            return null;
        }
    }

    /// <summary>
    /// Funcion para verificar si una factura esta valida en el Servicio de Impuestos
    /// </summary>
    /// <param name="UrlFac">Url de la factura</param>
    /// <returns>Estado de la factura</returns>
    public async Task<bool?> EsFacturaValida(string UrlFac)
    {
        try
        {
            using HttpResponseMessage response = await _httpClient.GetAsync(UrlFac);
            response.EnsureSuccessStatusCode();
            string result = await response.Content.ReadAsStringAsync();
            return result.Contains("<label>VALIDA</label>");
        }
        catch (Exception)
        {
            return null;
        }
    }
}
