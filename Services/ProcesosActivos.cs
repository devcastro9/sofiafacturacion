﻿namespace SofiaFacturacion.Services;

public class ProcesosActivos
{
    private static ProcesosActivos? _instance = null;

    private static readonly object _lockObject = new();

    private bool Activo = false;

    private ProcesosActivos() { }

    public static ProcesosActivos Instance
    {
        get
        {
            lock (_lockObject)
            {
                _instance ??= new ProcesosActivos();
                return _instance;
            }
        }
    }

    public bool ProcesoActivo()
    {
        Activo = true;
        return Activo;
    }
    public bool ProcesoTerminado()
    {
        Activo = false;
        return Activo;
    }
}
