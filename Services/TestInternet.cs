﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Threading.Tasks;

namespace SofiaFacturacion.Services;

public class TestInternet : ITestInternet, IDisposable
{
    private readonly List<string> Urls = new() { "8.8.8.8", "1.1.1.1" };
    private Ping? _ping = null;
    /// <summary>
    /// Funcion para verificar la existencia de conectividad a internet
    /// </summary>
    /// <returns>Un valor booleano que representa la conectividad</returns>
    public async Task<bool> ExisteInternet()
    {
        try
        {
            _ping = new Ping();
            List<IPStatus> estados = new();
            foreach (string item in Urls)
            {
                PingReply result = await _ping.SendPingAsync(item);
                estados.Add(result.Status);
            }
            return estados.Contains(IPStatus.Success);
        }
        catch (Exception)
        {
            return false;
        }
    }
    public void Dispose()
    {
        _ping?.Dispose();
        GC.SuppressFinalize(this);
    }
}
