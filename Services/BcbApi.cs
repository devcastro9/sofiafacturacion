﻿using SofiaFacturacion.Utility;
using SofiaFacturacion.ViewModels;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace SofiaFacturacion.Services;

public class BcbApi
{
    private readonly HttpClient _httpClient;
    public BcbApi(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }

    public async Task<IList<BcbUfvRes>?> GetUfv(DateTime fechaIni, DateTime fechaFin)
    {
        string fechaInicial = fechaIni.ToString("yyyy-MM-dd");
        string fechaFinal = fechaFin.ToString("yyyy-MM-dd");
        using HttpResponseMessage response = await _httpClient.GetAsync($"https://www.bcb.gob.bo/librerias/charts/ufv.php?cFecIni={fechaInicial}&cFecFin={fechaFinal}");
        response.EnsureSuccessStatusCode();
        // Deserializacion de la respuesta
        JsonSerializerOptions options = new()
        {
            ReferenceHandler = ReferenceHandler.IgnoreCycles
        };
        options.Converters.Add(new FormatDateTimeConverter("dd/MM/yyyy"));
        string ContentResponse = await response.Content.ReadAsStringAsync();
        return JsonSerializer.Deserialize<IList<BcbUfvRes>>(ContentResponse, options);
    }
}
