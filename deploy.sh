#!/bin/bash
# Ubicado en 192.168.3.150
# Usuario: developer Password: kernel
# Este archivo se encuentra en: /home/developer/Documentos
# Script de borrado y vuelta de crear online
contenedor_fac='facturacion-backend'
# Borrado de contenedores
sudo docker stop $(sudo docker ps -q)
sudo docker rm $(sudo docker ps -a -q)
sudo docker rmi $(sudo docker images -q)
# Borrado total (usese con precaucion)
sudo docker system prune -a -f
# Despliegue de facturacion
cd /home/developer/Documentos/sofiafacturacion
sudo docker build -t $contenedor_fac .
sudo docker run --name facturacion --restart unless-stopped -d -p 5000:5000 $contenedor_fac
