using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using SofiaFacturacion.Data;
using SofiaFacturacion.HostedServices;
using SofiaFacturacion.Services;
using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace SofiaFacturacion;

public class Program
{
    public static void Main(string[] args)
    {
        WebApplicationBuilder builder = WebApplication.CreateBuilder(args);
        // Adicion de servicios al contenedor IServiceCollection
        // Base de datos
        builder.Services.AddDbContext<AppDbContext>(options => options.UseSqlServer(builder.Configuration.GetConnectionString("Bdatos"), sqlOptions =>
        {
            sqlOptions.CommandTimeout(25);
            sqlOptions.EnableRetryOnFailure(maxRetryCount: 6, maxRetryDelay: TimeSpan.FromSeconds(20), errorNumbersToAdd: new List<int> { 4060 });
        }));
        // Adicion de Servicios
        builder.Services.AddSingleton<ITestInternet, TestInternet>();
        builder.Services.AddScoped<IEmailSmtp>(em => new EmailSmtp(builder.Configuration.GetValue<string>("Smtp:Server")!,
                builder.Configuration.GetValue<int>("Smtp:Port"),
                builder.Configuration.GetValue<string>("Smtp:SenderEmail")!,
                builder.Configuration.GetValue<string>("Smtp:Password")!,
                builder.Configuration.GetValue<string>("Smtp:Email")!,
                builder.Configuration.GetValue<string>("Smtp:ConCopy")!));
        builder.Services.AddScoped<ISiatFacturacion, SiatFacturacion>();
        // Adicion de HttpClient
        builder.Services.AddHttpClient<LibelulaApi>();
        builder.Services.AddHttpClient<AbstractApi>();
        builder.Services.AddHttpClient<ViewSiatFacEstado>();
        builder.Services.AddHttpClient<ApiLayer>();
        builder.Services.AddHttpClient<BcbApi>();
        // Adicion de HostedService
        builder.Services.AddHostedService<ProcGeneral>();
        builder.Services.AddHostedService<Facturador>();
        builder.Services.AddHostedService<ProcReAdicion>();
        builder.Services.AddHostedService<Anulador>();
        builder.Services.AddHostedService<ConsultaData>();
        builder.Services.AddHostedService<TipoCambioHs>();
        // Controller
        builder.Services.AddControllers()
                        .AddJsonOptions(options => options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);
        // Configuracion Swagger/OpenAPI
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen(options =>
        {
            options.SwaggerDoc("v1", new OpenApiInfo
            {
                Version = "v1",
                Title = "Facturacion electronica SOFIA",
                Description = "Se documentan todas las endpoints del backend"
            });
        });
        // Configuracion de CORS
        builder.Services.AddCors(options => options.AddDefaultPolicy(policy =>
        {
            policy.AllowAnyHeader()
                  .AllowAnyOrigin()
                  .AllowAnyMethod();
        }));
        // Construye la aplicacion configurada
        WebApplication app = builder.Build();

        // Configuracion de la canalizacion de solicitudes HTTP.
        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }
        // CORS
        app.UseCors();
        //app.UseHttpsRedirection();

        //app.UseAuthorization();
        //app.UseAuthentication();

        app.MapControllers();

        app.Run();
    }
}