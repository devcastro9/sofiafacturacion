﻿# Facturacion
Url de Factura CGE
https://pagos.libelula.bo//factura/89633a86f38b43e8be170976df29c14a94ee2379c2cc4244abfd9a26e92af821f2e4cf8ac526400490d6d80596a2e98b
Url de XML CGE
https://pagos.libelula.bo//factura/89633a86f38b43e8be170976df29c14a94ee2379c2cc4244abfd9a26e92af821f2e4cf8ac526400490d6d80596a2e98b?xml=1
## Comandos
```
echo 'Hola mundo'
```
Comandos de mapeados de facturacion:
```
Scaffold-DbContext "Server=tcp:192.168.3.133,1433;Database=ADMIN_EMPRESA;Trusted_Connection=True;Encrypt=False" -Provider Microsoft.EntityFrameworkCore.SqlServer -DataAnnotations -Context AppDbContext -ContextDir Data -OutputDir Models -NoOnConfiguring
-Tables
facConfiguracion,facDominioEmail,facEstado,facEstadoTipo,facKey,facRegistroEvento,facRegistroJson,
ao_ventas_cobranza_fac,
FacturaElec,FacturaElecDetalle,FacturaElecMetadato,FacturaElecAnulacion
FacturaLibelulaEvento
-Force
```
Comando de mapeado de facturacion valido:
```
Scaffold-DbContext "Server=tcp:192.168.3.211,1433;Database=PRUEBA;Trusted_Connection=True;Encrypt=False" -Provider Microsoft.EntityFrameworkCore.SqlServer -DataAnnotations -Context AppDbContext -ContextDir Data -OutputDir Models -NoOnConfiguring -Tables facConfiguracion,facDominioEmail,facEstado,facEstadoTipo,facKey,facRegistroEvento,facRegistroJson,facRegistroJsonTipo,ao_ventas_cobranza_fac,FacturaElec,FacturaElecDetalle,FacturaElecMetadato,FacturaElecAnulacion,FacturaElecConsulta,FacturaLibelulaEvento -Force
```
Modificado
```
_context.Entry(facEstado).State = EntityState.Modified;
```